from tableminer.tableparser import TableParser
from tableminer.textbox import TextBox
from tableminer.tablelayout import TableLine
import unittest

class TableParserTest(unittest.TestCase):
	def test_filter_lines_larger_than(self):
		vlines=[]
		vline = TableLine(84.14, 595.72, 84.86, 609.34)
		vlines.append(vline)
		vline = TableLine(84.14, 581.38, 84.86, 594.9399999999999)
		vlines.append(vline)
		vline = TableLine(84.14, 566.98, 84.86, 580.6)
		vlines.append(vline)
		vline = TableLine(84.14, 552.64, 84.86, 566.1999999999999)
		vlines.append(vline)
		return_vlines = TableParser.filter_lines_larger_than(vlines, y0=560)
		self.assertEqual(len(return_vlines), 1)
		for vline in return_vlines:
			print (vline.x0, vline.y0, vline.x1, vline.y1)

	def test_filter_lines_less_than(self):
		vlines=[]
		vline = TableLine(84.14, 595.72, 84.86, 609.34)
		vlines.append(vline)
		vline = TableLine(84.14, 581.38, 84.86, 594.9399999999999)
		vlines.append(vline)
		vline = TableLine(84.14, 566.98, 84.86, 580.6)
		vlines.append(vline)
		vline = TableLine(84.14, 552.64, 84.86, 566.1999999999999)
		vlines.append(vline)
		return_vlines = TableParser.filter_lines_less_than(vlines, y0=590)
		self.assertEqual(len(return_vlines), 1)
		for vline in return_vlines:
			print (vline.x0, vline.y0, vline.x1, vline.y1)

	def test_get_textbox_by_index(self):
		textboxes = []
		textboxes.append(TextBox(0,0,0,0, text="abc"))
		textboxes.append(TextBox(0,0,0,0, text="efg"))
		textboxes.append(TextBox(0,0,0,0, text="abc"))
		textboxes.append(TextBox(0,0,0,0, text="abc"))
		textboxes.append(TextBox(0,0,0,0, text="abc"))
		textboxes.append(TextBox(0,0,0,0, text="abc"))
		titles = ["efg"]
		textbox = TableParser.get_textbox_by_index(textboxes, titles)
		self.assertEqual(textbox.text,"efg")

	def test_parse_region(self):
		textboxes = []
		textboxes.append(TextBox(0,0,0,0, text="abc"))
		textboxes.append(TextBox(10,50,20,20, text="text1"))
		textboxes.append(TextBox(20,50,40,20, text="text2"))
		textboxes.append(TextBox(0,0,0,0, text="abc"))
		textboxes.append(TextBox(0,0,0,0, text="abc"))
		textboxes.append(TextBox(0,0,0,0, text="abc"))
		returned_textboxes = TableParser.parse_region(textboxes, 9, 49, 39, 10)
		self.assertEqual(len(returned_textboxes), 2)
		self.assertEqual(" ".join(textbox.text for textbox in returned_textboxes), "text1 text2")

if __name__ == '__main__':
    unittest.main()


import unittest
from tableminer.util import Util
from tableminer.textbox import TextBox
from tableminer.constants import Constants
class Test(unittest.TestCase):
	def test_merge_first_textboxes(self):
		print("test_merge_first_textboxes start")
		textboxes = [TextBox(58, 520, 62, 511, " ", 12), TextBox(67, 520, 123, 510, "  结算备付金 ", 12), TextBox(58, 505, 150, 495, "testing-text123", 12)]
		merged_textboxes = Util.merge_first_textboxes(textboxes)
		self.assertEqual(len(merged_textboxes), 2)
		self.assertEqual(merged_textboxes[0].x0, 67+Constants.CONSTANTS_SPACE_WIDTH*2)
		self.assertEqual("".join([textbox.text for textbox in merged_textboxes]), "结算备付金testing-text123")

	def test_merge_empty(self):
		print("case 2: merge empty textboxes")
		textboxes = [TextBox(58, 520, 62, 511, " ", 12), TextBox(67, 520, 123, 510, "  ", 12), TextBox(58, 505, 150, 495, "    ", 12)]
		merged_textboxes = Util.merge_first_textboxes(textboxes)
		self.assertEqual(len(merged_textboxes), 0)
		self.assertEqual(type(merged_textboxes), list)
if __name__ == '__main__':
	unittest.main()
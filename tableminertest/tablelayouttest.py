from tableminer.tablelayout import TableLayout
from tableminer.tablelayout import TableLine
import unittest

class TableLayoutTest(unittest.TestCase):
	def test_group_hlines(self):
		hlines = []
		hline = TableLine(88.4, 785.94, 532.02, 786.6600000001)
		hlines.append(hline)
		hline = TableLine(84.86, 609.4, 266.68, 610.12)
		hlines.append(hline)
		hline = TableLine(426.96, 609.4, 536.64, 610.12)
		hlines.append(hline)
		
		grouped_hlines = TableLayout._group_hlines(hlines)
		for line in grouped_hlines:
			print ("hline: x0=[{}], y0=[{}], x1=[{}], y1=[{}]".format(line.x0, line.y0, line.x1, line.y1))
		self.assertEqual(len(grouped_hlines),2)

	def test_group_vlines(self):
		vlines=[]
		vline = TableLine(84.14, 595.72, 84.86, 609.34)
		vlines.append(vline)
		vline = TableLine(84.14, 581.38, 84.86, 594.9399999999999)
		vlines.append(vline)
		vline = TableLine(84.14, 566.98, 84.86, 580.6)
		vlines.append(vline)
		vline = TableLine(84.14, 552.64, 84.86, 566.1999999999999)
		vlines.append(vline)

		vline = TableLine(266.68, 595.72, 267.40000000000003, 609.34)
		vlines.append(vline)
		vline = TableLine(266.68, 581.38, 267.40000000000003, 594.9399999999999)
		vlines.append(vline)
		vline = TableLine(266.68, 566.98, 267.40000000000003, 580.6)
		vlines.append(vline)
		vline = TableLine(266.68, 552.64, 267.40000000000003, 566.1999999999999)
		vlines.append(vline)
		vline = TableLine(266.68, 538.24, 267.40000000000003, 551.86)
		vlines.append(vline)

		vline = TableLine(266.68, 395.72, 267.40000000000003, 409.34)
		vlines.append(vline)
		vline = TableLine(266.68, 381.38, 267.40000000000003, 394.9399999999999)
		vlines.append(vline)
		vline = TableLine(266.68, 366.98, 267.40000000000003, 380.6)
		vlines.append(vline)
		vline = TableLine(266.68, 352.64, 267.40000000000003, 366.1999999999999)
		vlines.append(vline)
		vline = TableLine(266.68, 338.24, 267.40000000000003, 351.86)
		vlines.append(vline)


		vline = TableLine(280.68, 338.24, 267.40000000000003, 351.86)
		vlines.append(vline)

		grouped_vlines = TableLayout._group_vlines(vlines)
		for line in grouped_vlines:
			print ("vline: x0=[{}], y0=[{}], x1=[{}], y1=[{}]".format(line.x0, line.y0, line.x1, line.y1))
		self.assertEqual((len(grouped_vlines)),4)

	def test_group_vlines_hard(self):
		vlines=[
			TableLine(),

		]

		grouped_vlines = TableLayout._group_vlines(vlines)

	
if __name__ == '__main__':
	unittest.main()


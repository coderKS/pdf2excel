from tableminer.translation import Translation
from tableminer.tablecontent import TableContent
import unittest

class TranslationTest(unittest.TestCase):
	def test_set_dictionary(self):
		file_paths = ["translation/cf.csv"]
		translation = Translation()
		translation.set_dictionary(file_paths)
		self.assertTrue(len(translation.dictionary) > 0 )
		print (translation.dictionary)


if __name__ == '__main__':
	unittest.main()



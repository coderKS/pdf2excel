from tableminer.tablecontent import TableContent
from tableminer.textbox import TextBox
import unittest

class TableContentTest(unittest.TestCase):
	def when_init_table_size_should_be_zero(self):
		content = TableContent()
		self.assertEqual(len(content.table),0)

	def when_add_content_should_append_interacted_textbox_to_table(self):
		print ("when_add_content_should_append_interacted_textbox_to_table# start")
		textboxes = []
		textboxes.append(TextBox(0,0,0,0, text="abc"))
		textboxes.append(TextBox(10,50,20,20, text="text1"))
		textboxes.append(TextBox(25,50,30,20, text="text2"))
		textboxes.append(TextBox(0,0,0,0, text="abc"))
		textboxes.append(TextBox(0,0,0,0, text="abc"))
		textboxes.append(TextBox(0,0,0,0, text="abc"))
		content = TableContent()
		content.add_content([50,20],[9,22,31],textboxes)

		self.assertEqual(len(content.table),1)
		self.assertEqual(len(content.table[0]),2)
		self.assertEqual(content.table[0][0].text,"text1")
		self.assertEqual(content.table[0][1].text,"text2")

if __name__ == '__main__':
	unittest.main()
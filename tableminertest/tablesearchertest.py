from tableminer.tablesearcher import TableSearcher
from tableminer.tablesearcher import TableSearchResult
from tableminer.tablesearcher import Table
from tableminer.tablesearcher import EndIndex, StartIndex
import unittest

class TableSearcherTest(unittest.TestCase):
	def test_parse_page_content_into_indexes(self):
		content = "我们的责任是在执行审计工作的基础上对财务报表发表审计意见。"
		page_num = 65
		startIndexes =["我们", "执行审计", "按照中国", "中国注册会计师"]
		endIndexes = ["责任","审计意见","index"]
		indexes = TableSearcher._parse_page_content_into_indexes(content, page_num, startIndexes, endIndexes)
		self.assertEqual(len(indexes), 4)

class TableSearchResultTest(unittest.TestCase):
	def test_filter_index(self):
		indexes = [ StartIndex(1, "s1"),
								EndIndex(5,"e1"),
								StartIndex(5,"s2"),
								EndIndex(7,"e2")]

		searchResult = TableSearchResult(None)
		searchResult.indexes = indexes
		searchResult.filter_indexes()

		self.assertEqual(searchResult.isFound, True)
		self.assertEqual(searchResult.startPage, 5)
		self.assertEqual(searchResult.endPage, 7)

if __name__ == '__main__':
	unittest.main()
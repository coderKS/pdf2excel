#!/usr/bin/python3
from db.entity import ItemValue
from tableminer.tablecontent import TableContent
import sys, os
import logging
# this is a helper class
# help converting a tablecontent instance to the list of ItemValue array

logger = logging.getLogger("tableminer")

class TableContentConvertor:
	def __init__(self):
		self.prev_cell_level = 0
		# self.prev_level_sequence = ""
		self.prev_level_index = 0

	def _get_sequence(self, col, row, level):
		sequence = str('{0:05d}'.format(col)) + str('{0:05d}'.format(row))
		if level > self.prev_cell_level:
			self.prev_level_index += 1
		elif level < self.prev_cell_level:
			self.prev_level_index -= 1 
		sequence += str('{0:05d}'.format(self.prev_level_index))
		self.prev_cell_level = level
		return sequence

	def convert_to_itemvalues(self, table_id, tablecontent):
		item_values = []
		for row in tablecontent.table: #tablecontent.table: an 2D array of Table Cell
			item = None
			item_coord_00 = None
			item_coord_01 = None
			item_coord_10 = None
			item_coord_11 = None

			for cell in row: # row: list of Table Cell
				if cell.row_index == 0:
					# skip the table header
					continue

				if cell.col_index == 0:
					item_cell = cell
					item = "".join([textbox.text for textbox in cell.textboxes])
					if len(cell.textboxes) > 0:
						item_coord_00 = cell.textboxes[0].x0
						item_coord_01 = cell.textboxes[0].y0
						item_coord_10 = cell.textboxes[-1].x1
						item_coord_11 = cell.textboxes[-1].y1

				else: # col_index not equal 0
					value = None
					value_coord_00 = None
					value_coord_01 = None
					value_coord_10 = None
					value_coord_11 = None
					currency = None
					sequence = self._get_sequence(cell.col_index, cell.row_index, item_cell.level)
					value = "".join([textbox.text for textbox in cell.textboxes])

					logger.debug("len(tablecontent.table[0])=[{}]".format(len(tablecontent.table[0])))
					logger.debug("cell.col_index=[{}]".format(cell.col_index))
					logger.debug("item=[{}], value=[{}]".format(item, value))
					logger.debug("cell.col_index=[{}], cell.row_index=[{}], item_cell.level=[{}]".format(cell.col_index, cell.row_index, item_cell.level))
					logger.debug("sequence=[{}]\n".format(sequence))
					
					if (cell.col_index >= len(tablecontent.table[0])):
						logger.warn("col_index: [{}] index out of bound for item: [{}] and value: [{}]".format(cell.col_index, item, value))
						# prevent index out of bound due to tableparser wrongly add cells
						continue

					column = "".join([textbox.text for textbox in tablecontent.table[0][cell.col_index].textboxes])
					column_index = cell.col_index
					
					

					if len(cell.textboxes) > 0:
						value_coord_00 = cell.textboxes[0].x0
						value_coord_01 = cell.textboxes[0].y0
						value_coord_10 = cell.textboxes[-1].x1
						value_coord_11 = cell.textboxes[-1].y1
					item_value = ItemValue(None, item, item_coord_00, item_coord_01, item_coord_10, item_coord_11,
													value, value_coord_00, value_coord_01, value_coord_10, value_coord_11,
													currency, table_id, sequence, column, column_index)
					item_values.append(item_value)
			# end for cell in row
		# end for row in tablecontent.table
		return item_values





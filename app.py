#!/usr/bin/python
# -*- coding: utf-8 -*-
import codecs
import configparser
import json
import logging
import os
from os.path import basename

from db.connectionManager import ConnectionManager
from db.dao import CompanyDao, AnnualReportDao, ReportTableDao, ItemValueDao
from db.entity import Company, AnnualReport, ReportTable
from tablecontentconvertor import TableContentConvertor
from tableminer.logger import Logger
from tableminer.report import ReportUtil


def get_config():
    return json.load(codecs.open('config.json', 'r', 'utf-8-sig'))


# return list of path of files in the directory
# params
def get_file_paths(directory):
    paths = []
    for filename in os.listdir(directory):
        paths.append(os.path.join(directory, filename))
    return paths


def get_basename(filepath):
    return basename(filepath)


def get_tables(tables_config):
    tables = []
    for config in tables_config:
        if config["is_active"]:
            tables.append(Table(config["table_name"], config["table_start_indexes"],
                                config["table_end_indexes"], config["suffix"]))
    return tables


# parameters
config = get_config()
input_directory = config["input_directory"]
output_directory = config["output_directory"]
tables_config = config["tables"]
export_format = "xlsx" if config["export"] == "xlsx" else "csv"
debug = config["debug"]
log_file_directory = config["log_file_directory"]
upload_to_mysql = config["upload_to_mysql"]
export_to_file = config["export_to_file"]

# db
config = configparser.ConfigParser()
config.sections()
config.read('db/db.ini')
db_address = config['db']['address']
db_port = config['db']['port']
db_username = config['db']['username']
db_password = config['db']['password']
db_name = config['db']['dbname']
db_conn = ConnectionManager(db_address, db_address, db_username, db_password, db_name)
companyDao = CompanyDao(db_conn)
annualReportDao = AnnualReportDao(db_conn)
reportTableDao = ReportTableDao(db_conn)
itemValueDao = ItemValueDao(db_conn)

# set logger
_logger = Logger(debug=debug, filepath=log_file_directory)
_logger.setLogger()
logger = logging.getLogger("tableminer")

from tableminer.tablesearcher import TableSearcher, Table
from tableminer.tableparser import TableParser
from tableminer.tablewriter import TableWriter

# get all filepath in the input directory
file_paths = get_file_paths(input_directory)

# convert table config to table objects
tables = get_tables(tables_config)

#     file_paths = ["data/古越龙山 (600059) 2013-4Q (R) (chi).pdf"]
# translation = Translation()
# translation.set_dictionary(["translation/cf.csv",
# 						"translation/is.csv"])
# logger.info("dictionary size={}".format(len(translation.dictionary)))

for file_path in file_paths:
    logger.info("-----------------------------")
    logger.info("| " + file_path)
    logger.info("-------------------------------")

    searcher = TableSearcher(file_path)
    search_results = searcher.search_tables(tables)
    if search_results == None:
        continue

    if upload_to_mysql:
        # create company instance
        company_name = file_path.split('/')[-1].split('(')[0].strip(' ')
        company = companyDao.get_by_name(company_name)
        if not company:
            company_exchange = 'Shenzhen Stock Exchange'
            company_ticker = company_name
            company = Company(None, company_name, company_exchange, company_ticker)
            company_id = companyDao.add(company)
        else:
            company_id = company.company_id

    if upload_to_mysql:
        # create or update annual report instance
        file_name = get_basename(file_path)
        report = annualReportDao.get_by_name(file_name)
        if not report:
            year = file_name[file_name.find('-') - 4:file_name.find('-')]
            start_period = file_name[file_name.find('Q') - 1: file_name.find('Q') + 1]
            end_period = start_period
            report_type = ReportUtil.get_report_type(file_path)
            report = AnnualReport(None, file_name, file_path, year, start_period, end_period, report_type, company_id)
            report_id = annualReportDao.add(report)
        else:
            report_id = report.report_id

    for result in search_results:
        if result.isFound:
            # if result is found
            logger.info("=================================")
            logger.info("Found table[%s] from page [%d] to [%d]" %
                        (result.table.name, result.startPage, result.endPage)
                        )
            logger.info("Parsing table[{0}]...".format(result.table.name))

            output_file_path = output_directory + get_basename(file_path) + result.table.suffix + "." + export_format

            # get table content using TableParser
            tablecontent = TableParser.parse_table_content(file_path, result)

            if upload_to_mysql:
                # create table instance
                table = reportTableDao.get_by_name(result.table.name, report_id)
                if not table:
                    table = ReportTable(None, result.table.name, result.startPage, result.endPage,
                                        tablecontent.table_info, report_id)
                    table_id = reportTableDao.add(table)
                else:
                    table_id = table.table_id

            if tablecontent:
                # translation.add_row_items(tablecontent)
                # export the table
                if export_to_file:
                    writer = TableWriter(output_file_path, tablecontent, export_format)
                    writer.export()
                    logger.info("[OK] output_path=[%s]" %
                                (output_file_path)
                                )

                if upload_to_mysql:
                    # delete all itemvalue of the table in db
                    itemValueDao.delete_table(table_id)
                    # create ItemValue for the table in db
                    convertor = TableContentConvertor()
                    itemvalues = convertor.convert_to_itemvalues(table_id, tablecontent)
                    itemValueDao.add_all(itemvalues)

                logger.info("=================================")

            else:
                logger.info("Fail to export the file")
                logger.info("=================================")

        else:
            # result not found
            logger.info("table [{}] not found".format(result.table.name))
# translation.export_result("translation/result.xlsx")

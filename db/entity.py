#!/usr/bin/python3

class Company():
	def __init__(self, id, name, exchange, ticker):
		self.company_id = id
		self.company_name = name
		self.exchange = exchange
		self.ticker = ticker

class AnnualReport():
	def __init__(self, id, fname, fpath, year, start, end, type, cid):
		self.report_id = id
		self.file_name = fname
		self.file_path = fpath
		self.year			 = year
		self.start_period = start
		self.end_period = end
		self.type				= type
		self.company_id = cid

class ItemValue():
	def __init__(self, id, item, item_coord_00, item_coord_01, item_coord_10, item_coord_11,
								value, value_coord_00, value_coord_01, value_coord_10, value_coord_11,
								currency, table_id, sequence, column, column_index):
		self.item_value_id = id
		self.item = item
		self.item_coord_00 = item_coord_00
		self.item_coord_01 = item_coord_01
		self.item_coord_10 = item_coord_10
		self.item_coord_11 = item_coord_11
		self.value = value
		self.value_coord_00 = value_coord_00
		self.value_coord_01 = value_coord_01
		self.value_coord_10 = value_coord_01
		self.value_coord_11 = value_coord_11
		self.currency = currency
		self.table_id = table_id
		self.sequence = sequence
		self.column = column
		self.column_index = column_index

class ReportTable():
	def __init__(self, id, name, start_page, end_page, table_header, rid):
		self.table_id = id
		self.table_name = name
		self.start_page = start_page
		self.end_page = end_page
		self.table_header = table_header
		self.report_id = rid

class Translation():
		def __init__(self, id, cid, chinese, english):
			self.translation_id = id
			self.company_id = cid
			self.chinese = chinese
			self.english = english

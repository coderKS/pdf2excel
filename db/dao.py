#!/usr/bin/python3

from db.entity import Company, AnnualReport, ItemValue, ReportTable


# class Dao is in interface
class Dao:
    def get(self, id):
        raise NotImplementedError

    def add(self, obj):
        raise NotImplementedError

    def delete(self, id):
        raise NotImplementedError

    def update(self, id, obj):
        raise NotImplementedError

    def get_list(self):
        raise NotImplementedError


class CompanyDao(Dao):
    def __init__(self, connection_manager):
        self.connection_manager = connection_manager

    def get(self, id):
        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        sql = "SELECT * FROM COMPANY_TABLE WHERE COMPANY_ID = {}".format(id)
        print("sql = %s" % sql)

        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                cid = row[0]
                cname = row[2]
                exchange = row[4]
                ticker = row[5]
                print("cid = %s,cname = %s,exchange = %d,ticker = %s" % \
                      (cid, cname, exchange, ticker))
                company = Company(cid, cname, exchange, ticker)
                return company

        except:
            print("Error: unable to fetch data")

        return None

    def get_by_name(self, name):
        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        sql = "SELECT * FROM COMPANY_TABLE WHERE COMPANY_NAME_CN = '{}'".format(name)
        print("sql = %s" % sql)

        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                cid = row[0]
                cname = row[2]
                exchange = row[4]
                ticker = row[5]
                # print ("cid = %s,cname = %s,exchange = %s,ticker = %s" % \
                # 	(cid, cname, exchange, ticker))
                company = Company(cid, cname, exchange, ticker)
                return company

        except Exception as e:
            print(e)
            print("Error: unable to fetch data")

        return None

    def add(self, obj):
        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        sql = """INSERT INTO COMPANY_TABLE (COMPANY_NAME_CN, EXCHANGE, TICKER) 
                VALUES('{}', '{}', '{}')""".format(obj.company_name, obj.exchange, obj.ticker)

        print("sql = %s" % (sql))
        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            db.commit()
            return cursor.lastrowid

        except Exception as e:
            print(e)
            print("Error: unable to add data")

    def delete(self, id):
        raise NotImplementedError

    def update(self, id, obj):
        if not type(obj) == 'Company':
            print('obj must be type of Company')

        company = self.get(id)
        if not company:
            print('id not found')

        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        sql = """UPDATE COMPANY_TABLE SET COMPANY_NAME_CN = '{}', EXCHANGE = '{}', TICKER = '{}' 
                WHERE COMPANY_ID = {}""".format(obj.company_name, obj.exchange, obj.ticker, obj.company_id)

        print("sql = %s" % (sql))
        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            db.commit()

        except Exception as e:
            print(e)
            print("Error: unable to add data")

    def get_list(self):
        raise NotImplementedError


class AnnualReportDao(Dao):
    def __init__(self, connection_manager):
        self.connection_manager = connection_manager

    def get(self, id):
        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        sql = "SELECT * FROM REPORT_TABLE WHERE REPORT_ID = '{}'".format(id)
        print("sql = %s" % sql)

        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                rid = row[0]
                cid = row[1]
                fname = row[2]
                fpath = row[3]
                start = row[4]
                end = row[4]
                year = row[5]
                type = row[8]

                # print ("cid = %s,cname = %s,exchange = %s,ticker = %s" % \
                # 	(cid, cname, exchange, ticker))
                report = AnnualReport(rid, fname, fpath, year, start, end, type, cid)
                return report
        except Exception as e:
            print(e)
            print("Error: unable to fetch data")

        return None

    def get_by_name(self, name):
        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        sql = "SELECT * FROM REPORT_TABLE WHERE FILE_NAME = '{}'".format(name)
        print("sql = %s" % sql)

        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                rid = row[0]
                cid = row[1]
                fname = row[2]
                fpath = row[3]
                start = row[4]
                end = row[4]
                year = row[5]
                type = row[8]

                # print ("cid = %s,cname = %s,exchange = %s,ticker = %s" % \
                # 	(cid, cname, exchange, ticker))
                report = AnnualReport(rid, fname, fpath, year, start, end, type, cid)
                return report
        except Exception as e:
            print(e)
            print("Error: unable to fetch data")

        return None

    def add(self, obj):
        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        sql = """INSERT INTO REPORT_TABLE (FILE_NAME, FILE_PATH, PERIOD, COMPANY_ID, YEAR, REPORT_TYPE) 
                VALUES('{}', '{}', '{}', '{}', '{}', '{}')""".format(obj.file_name, obj.file_path,
                                                                     obj.start_period, obj.company_id,
                                                                     obj.year, obj.type)

        print("sql = %s" % (sql))
        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            db.commit()
            return cursor.lastrowid

        except Exception as e:
            print(e)
            print("Error: unable to add data")

    def update(self, id, obj):
        if not type(obj) == 'AnnualReport':
            print('obj must be type of AnnualReport')

        report = self.get(id)
        if not report:
            print('id not found')

        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        sql = """UPDATE REPORT_TABLE SET FILE_NAME = '{}', FILE_PATH = '{}', YEAR = '{}', PERIOD = '{}', COMPANY_ID = '{}', REPORT_TYPE = '{}' 
                WHERE REPORT_ID = {}""".format(obj.file_name, obj.file_path, obj.year, obj.start_period,
                                               obj.company_id, obj.type, obj.report_id)

        print("sql = %s" % (sql))
        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            db.commit()

        except Exception as e:
            print(e)
            print("Error: unable to add data")

    def delete(self, id):
        pass

    def get_list(self):
        raise NotImplementedError


class ReportTableDao(Dao):
    def __init__(self, connection_manager):
        self.connection_manager = connection_manager

    def get(self, id):
        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        sql = "SELECT * FROM TABLE_TABLE WHERE TABLE_ID = '{}'".format(id)
        print("sql = %s" % sql)

        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                tid = row[0]
                report_id = row[1]
                table_name = row[2]
                start_page = row[3]
                end_page = row[4]
                table_header = row[5]

                # print ("cid = %s,cname = %s,exchange = %s,ticker = %s" % \
                # 	(cid, cname, exchange, ticker))
                reportTable = ReportTable(tid, table_name, start_page, end_page, table_header, report_id)
                return reportTable
        except Exception as e:
            print(e)
            print("Error: unable to fetch data")

        return None

    def get_by_name(self, name, rid):
        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        sql = "SELECT * FROM TABLE_TABLE WHERE TABLE_NAME = '{}' AND REPORT_ID = '{}'".format(name, rid)
        print("sql = %s" % sql)

        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                tid = row[0]
                report_id = row[1]
                table_name = row[2]
                start_page = row[3]
                end_page = row[4]
                table_header = row[5]

                # print ("cid = %s,cname = %s,exchange = %s,ticker = %s" % \
                # 	(cid, cname, exchange, ticker))
                table = ReportTable(tid, table_name, start_page, end_page, table_header, report_id)
                return table
        except Exception as e:
            print(e)
            print("Error: unable to fetch data")

        return None

    def add(self, obj):
        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        sql = """INSERT INTO TABLE_TABLE (TABLE_NAME, START_PAGE, END_PAGE, TABLE_HEADER, REPORT_ID) 
                VALUES('{}', '{}', '{}', '{}', '{}')""".format(obj.table_name, obj.start_page, obj.end_page,
                                                               obj.table_header, obj.report_id)

        print("sql = %s" % (sql))
        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            db.commit()
            return cursor.lastrowid

        except Exception as e:
            print(e)
            print("Error: unable to add data")

    def update(self, id, obj):
        pass

    def get_list(self):
        pass

    def delete(self, id):
        pass


class ItemValueDao(Dao):
    def __init__(self, connection_manager):
        self.connection_manager = connection_manager

    def get(self, id):
        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        sql = "SELECT * FROM ITEM_VALUE_TABLE WHERE ITEM_VALUE_ID = '{}'".format(id)
        print("sql = %s" % sql)

        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                iid = row[0]
                table_id = row[1]
                item = row[2]
                item_coord_00 = row[6]
                item_coord_01 = row[7]
                item_coord_10 = row[8]
                item_coord_11 = row[9]
                value = row[10]
                value_coord_00 = row[13]
                value_coord_01 = row[14]
                value_coord_10 = row[15]
                value_coord_11 = row[16]
                currency = row[17]
                # sequence = row[13]
                column = row[18]
                column_index = row[19]

                # print ("cid = %s,cname = %s,exchange = %s,ticker = %s" % \
                # 	(cid, cname, exchange, ticker))
                itemValue = ItemValue(iid, item, item_coord_00, item_coord_01, item_coord_10, item_coord_11,
                                      value, value_coord_00, value_coord_01, value_coord_10, value_coord_11,
                                      currency, table_id, None, column, column_index)
                return itemValue
        except Exception as e:
            print(e)
            print("Error: unable to fetch data")

        return None

    def add(self, obj):
        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        if obj.value:
            sql = """INSERT INTO ITEM_VALUE_TABLE (item_name, Item_coord_00, Item_coord_01, Item_coord_10, Item_coord_11, value_coord_00, value_coord_01, value_coord_10, value_coord_11, value_content, table_id, value_column_item, value_column_index) 
            VALUES('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')""" \
                .format(obj.item, obj.item_coord_00, obj.item_coord_01, obj.item_coord_10, obj.item_coord_11,
                        obj.value_coord_00, obj.value_coord_01, obj.value_coord_10, obj.value_coord_11, obj.value,
                        obj.table_id, obj.column, obj.column_index)
        else:
            sql = """INSERT INTO ITEM_VALUE_TABLE (Item_name, Item_coord_00, Item_coord_01, Item_coord_10, Item_coord_11, Table_id, value_column_item, value_column_index) VALUES('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')""" \
                .format(obj.item, obj.item_coord_00, obj.item_coord_01, obj.item_coord_10, obj.item_coord_11,
                        obj.table_id, obj.column, obj.column_index)

        # print ("sql = %s" % (sql))
        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            db.commit()
            return cursor.lastrowid

        except Exception as e:
            print(e)
            print("Error: unable to add data")

    def add_all(self, objs):
        for obj in objs:
            self.add(obj)

    def delete(self, id):
        pass

    def update(self, id, obj):
        pass

    def get_list(self):
        pass

    def delete_table(self, table_id):
        db = self.connection_manager.get_connection()
        cursor = db.cursor()
        sql = """DELETE FROM ITEM_VALUE_TABLE WHERE Table_id = '{}'""".format(table_id)

        print("sql = %s" % (sql))
        # TODO: prevent SQL injection
        try:
            cursor.execute(sql)
            db.commit()

        except Exception as e:
            print(e)
            print("Error: unable to add data")

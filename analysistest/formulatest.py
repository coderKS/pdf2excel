import unittest
from analysis.formula import Formula
class FormulaTest(unittest.TestCase):
	def test_compile(self):
		formula = Formula('={a}+{b}+{c}')
		items_map = dict()
		items_map['a'] = 1
		items_map['b'] = 2
		items_map['c'] = 3
		text = formula.compile(items_map, 'C')
		self.assertEqual(text, '=C1+C2+C3')

	def test_search_bracket_content(self):
		text = '={a}+{b}+{c}'
		bracket_content = Formula._search_bracket_content(text)
		self.assertEqual(bracket_content, 'a')

	def test_search_bracket_content_when_not_found_should_return_none(self):
		text = '=C1+C2+C3'
		bracket_content = Formula._search_bracket_content(text)
		self.assertEqual(bracket_content, None)
	

import unittest
from analysis.query import AnnualQuery, SuppQuery, QuarterQuery, QueryFactory
from analysis.connectionManager import ConnectionManager

class QueryTest(unittest.TestCase):
	def test_can_return_query_object(self):
		factory = QueryFactory(None)
		query = factory.get_query('annual')
		self.assertTrue(isinstance(query, AnnualQuery))
		query = factory.get_query('supp')
		self.assertTrue(isinstance(query, SuppQuery))
		query = factory.get_query('quarter')
		self.assertTrue(isinstance(query, QuarterQuery))



	

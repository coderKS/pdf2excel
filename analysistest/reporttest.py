import unittest
from analysis.formula import Formula
from analysis.report import Report, ReportConvertor, ReportBuilder
class ReportConvertorTest(unittest.TestCase):
	def test_parse_array(self):
		report = Report()
		report.headers = ['h1', 'h2', 'h3']
		report.row_items = ['r1', 'r2', 'r3']
		report.main_contents = [
			['01', '02', '03'],
			['04', '05', '06'],
			['07', '08', '09']
		]

		rows = ReportConvertor.parse_array(report)
		self.assertEqual(rows, [
				['', 'h1', 'h2', 'h3'],
				['r1', '01', '02', '03'],
				['r2', '04', '05', '06'],
				['r3', '07', '08', '09']
			])

	def test_parse_array_when_invalid_return_none(self):
		report = Report()
		report.headers = ['h1', 'h2', 'h3']
		report.row_items = ['r1', 'r2', 'r3']
		report.main_contents = [
			['01', '02', '03'],
			['04', '05', '06']
		]

		rows = ReportConvertor.parse_array(report)
		self.assertEqual(rows, None)


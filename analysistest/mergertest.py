import unittest
from analysis.merger import Merger
class MergerTest(unittest.TestCase):
	def test_normailize(self):
		print ("test_normailize# start")
		text = '1. xxx'
		normalized_text = Merger.normalize(text)
		self.assertEqual(normalized_text, 'xxx')

	def test_merge_row_item(self):
		print ("test_merge_row_item# start")
		latest_rows = [
			['', 'col1', 'col2'],
			['1. xxx', 'N/A', '100'],
			['2. yyy', '200', '200']
		]
		other_rows = [
			['2. xxx', '80', 'N/A'],
			['3. zzz', '100', 'N/A']
		]
		merged_latest_rows, merged_other_rows = Merger.merge(latest_rows, other_rows)

		print ("test_merge_row_item# merged_latest_rows=[{}], merged_other_rows=[{}]".format(merged_latest_rows, merged_other_rows))
		self.assertEqual(merged_latest_rows, [
			['', 'col1', 'col2'],
			['1. xxx', '80', '100'],
			['2. yyy', '200', '200']
		]);
		self.assertEqual(merged_other_rows, [
			['3. zzz', '100', 'N/A']
		]);

	def test_merge_similar_row_item(self):
		print ("test_merge_similar_row_item# start")
		latest_rows = [
			['', 'col1', 'col2'],
			['1. xxx', 'N/A', '100'],
			['2. yyy', '200', '200'],
			['3. zz.', 'N/A', '300'],
		]
		other_rows = [
			['2. xxx', '80', 'N/A'],
			['3. zzz', '100', 'N/A']
		]

		similar_items = ["zz.", "zzz"]
		merged_latest_rows, merged_other_rows = Merger.merge_similar(latest_rows, other_rows, similar_items)

		print ("test_merge_row_item# merged_latest_rows=[{}], merged_other_rows=[{}]".format(merged_latest_rows, merged_other_rows))
		self.assertEqual(merged_latest_rows, [
			['', 'col1', 'col2'],
			['1. xxx', 'N/A', '100'],
			['2. yyy', '200', '200'],
			['3. zz.', '100', '300']
		]);
		self.assertEqual(merged_other_rows, [
			['2. xxx', '80', 'N/A']
		]);


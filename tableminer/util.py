from tableminer.textbox import TextBox
from tableminer.constants import Constants
class Util:
	# return boolean if text consists of all space character
	def is_space_text(text):
		return len(text.strip(" ")) == 0

	def merge_first_textboxes(textboxes):
		merged_textboxes = []

		is_text_appear = False
		for textbox in textboxes:
			if Util.is_space_text(textbox.text):
				continue
			elif not is_text_appear:
				# calculate number of space character on the left hand of the textbox
				num_l_space = len(textbox.text) - len(textbox.text.lstrip(" "))
				textbox.text = textbox.text.strip(" ")
				textbox.x0 += Constants.CONSTANTS_SPACE_WIDTH * num_l_space
				is_text_appear = True

			elif is_text_appear:
				textbox.text = textbox.text.strip(" ")
			merged_textboxes.append(textbox)

		# end for textbox in textboxes
		return merged_textboxes

	# input: list of table cell object
	# output: boolean
	# True if all the element is empty string or space characters
	# False otherwise
	def is_row_empty(row):
		if len(row) == 0:
			return True
		for cell in row:
			for textbox in cell.textboxes:
				if len(textbox.text.strip(" ")) > 0:
					return False
		return True

#!/usr/bin/python
# -*- coding: utf-8 -*-
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.layout import LAParams, LTRect, LTTextBox, LTTextLine, LTFigure, LTCurve
from pdfminer.converter import PDFPageAggregator
from tableminer.textbox import TextBox
from tableminer.constants import Constants
from itertools import islice
import logging
import operator
import sys, os

logger = logging.getLogger("tableminer")
class TableLineGroup:
 	def __init__(self, x0, y0, x1, y1):
 		self.x0 = x0
 		self.y0 = y0
 		self.x1 = x1
 		self.y1 = y1
 		self.width = x1 - x0
 		self.height = y1 - y0 
 		self.hlines = hlines # list of Horizontal TableLine
 		self.vlines = vlines # list of Vertical TableLine

 	def group_line(hlines, vlines):
 		# find the vertical lines
 		# map the vlines into 
 		return None



class TableLine:
	def __init__(self, x0, y0, x1, y1):
		self.x0 = x0
		self.y0 = y0
		self.x1 = x1
		self.y1 = y1

	def __repr__(self):
		return "TableLine: x0=[{}], y0=[{}], x1=[{}], y1=[{}]".format(self.x0, self.y0, self.x1, self.y1)

# TableLayout is responsible for parsing all the line object and text object in a page of pdf
class TableLayout:
	def __init__(self, page, pagenum):
		rsrcmgr = PDFResourceManager()
		laparams = LAParams(line_overlap=Constants.CONSTANTS_LINE_OVERLAP,
												 char_margin=Constants.CONSTANTS_CHAR_MARGIN, 
												 line_margin=Constants.CONSTANTS_LINE_MARGIN, 
												 word_margin=Constants.CONSTANTS_WORD_MARGIN,
												 boxes_flow=Constants.CONSTANTS_BOXES_FLOW,
												 detect_vertical=Constants.CONSTANTS_DETECT_VERTICAL,
												 all_texts=Constants.CONSTANTS_ALL_TEXTS)
		device = PDFPageAggregator(rsrcmgr, laparams=laparams)
		interpreter = PDFPageInterpreter(rsrcmgr, device)
		interpreter.process_page(page)
		layout = device.get_result()
		self.pagenum = pagenum
		self.layout = layout
		self.texts = [] # array of textbox object
		self.hlines = [] # array of LTCurve object with height = 0
		self.vlines = [] # array of LTCurve object with width = 0

		self._parse_layout(layout) # set textbox
		self._sort_textboxes() # sort the textbox by y0 then by x0
		
		# parse line
		self.hlines = self._parse_hlines() # set hlines
		self.vlines = self._parse_vlines() # set vliens

		# group line
		self.hlines = TableLayout._group_hlines(self.hlines) # group LTCurve object to TableLine object
		self.vlines = TableLayout._group_vlines(self.vlines) # group LTCurve object to TableLine object

		self._print_layout() # for debugging

	def _print_layout(self):
		for textbox in self.texts:
			text = textbox.text

			logger.debug("	text=[%s], coord=[%6d, %6d, %6d, %6d]" %
					(text, textbox.x0, textbox.y0, textbox.x1, textbox.y1)
				)

		logger.debug("\thlines")
		for hline in self.hlines:
			logger.debug("\t\tx0=[{0}], y0=[{1}], x1=[{2}], y1=[{3}]".format(hline.x0, hline.y0, hline.x1, hline.y1))

		logger.debug("\tvlines")
		for vline in self.vlines:
			logger.debug("\t\tx0=[{0}], y0=[{1}], x1=[{2}], y1=[{3}]".format(vline.x0, vline.y0, vline.x1, vline.y1))

	def _sort_textboxes(self):
		if len(self.texts) <= 0:
			return

		# sort the textboxes base on the y-coord then x-coord
		for textbox in self.texts:
			textbox.y0 *= -1

		self.texts = sorted(self.texts, key=operator.attrgetter('y0', 'x0'))

		for textbox in self.texts:
			textbox.y0 *= -1


	def _add_text(self, obj):
		text = obj.get_text().replace("\n","")
		if text != None and text != "" and text:
			textbox = TextBox(obj.bbox[0], obj.bbox[3], obj.bbox[2], obj.bbox[1], text, page=self.pagenum)
			self.texts.append(textbox)

	# find all the TextBox object in pdf
	def _parse_layout(self, layout):
		logger.debug("start")
		"""Function to recursively parse the layout tree."""
		for lt_obj in layout:
			if isinstance(lt_obj, LTTextBox):
				for obj in lt_obj._objs:
					self._add_text(obj)

			elif isinstance(lt_obj, LTTextLine):
				self._add_text(lt_obj)

			elif isinstance(lt_obj, LTFigure):
				self._parse_layout(lt_obj)  # Recursive

	def _group_vlines(vlines):
		logger.debug("start - vlines length=[{}]".format(len(vlines)))
		
		# check vlines size
		if len(vlines) == 0:
			return vlines

		# sort vlines by x0 first then by y0
		vlines = sorted(vlines, key=operator.attrgetter('x0','y0'))

		grouped_vlines = []
		vlines_2d = []
		vlines_1d = []

		try:
			########################################################
			# Re-construct the 1d array to 2d array by value of x0 # 
			########################################################

			# store the previous vlines x0 value
			prev_x0_val = vlines[0].x0
			# add the first vline to vlines_1d list
			vlines_1d.append(vlines[0])
			# previous insert position
			prev_insert_pos = 0

			for i in range(1,len(vlines)):
				# compare the current vline x0 value to the previous x0 value
				# if difference between the current vline x0 and the previous one is small than the min diff,
				# append it to the vlines_1d list
				# otherwise append vlines_1d to the vlines_2d list and reset vlines_1d and 
				#		add current vlines to reset vlines_1d
				if (vlines[i].x0 == prev_x0_val):
					vlines_1d.append(vlines[i])
					prev_x0_val = vlines[i].x0

				elif abs(prev_x0_val - vlines[i].x0) <= Constants.CONSTANTS_TABLE_VLINES_HORIZONTAL_MIN_DIFF:
					# insert the vlines[i] into the vlines_1d position j which vlines[i].y0 is bearly larger than vlines_1d[j].y0
					is_inserted = False
					for j in range(prev_insert_pos, len(vlines_1d)):
						if vlines_1d[j].y0 > vlines[i].y0:
							vlines_1d.insert(j, vlines[i])		
							prev_insert_pos = j+1 # update
							is_inserted = True
							break

					if not is_inserted:
						vlines_1d.insert(len(vlines_1d), vlines[i]) # append at the end of vlines_1d
						prev_insert_pos = len(vlines_1d)+1 # update

				else:
					vlines_2d.append(vlines_1d)
					vlines_1d = [vlines[i]]
					prev_insert_pos = 0 # reset 
					prev_x0_val = vlines[i].x0
				# set current vline x0 value to previous x0 value
				
			# add the remaining vline_1d to the vlines_2d list
			vlines_2d.append(vlines_1d)

			for vlines_1d in vlines_2d:
				table_line = TableLine(0,0,0,0)
				table_line.x0 = vlines_1d[0].x0
				table_line.x1 = vlines_1d[0].x1
				table_line.y0 = vlines_1d[0].y0
				table_line.y1 = vlines_1d[0].y1
				for i in range(len(vlines_1d)-1):
					if abs(vlines_1d[i+1].y0 - vlines_1d[i].y1) < Constants.CONSTANTS_TABLE_MIN_MARGIN:
						# line i and i+1 should belongs to the same line
						table_line.y1 = vlines_1d[i].y1

					else:
						# the line i+1 should be in another table
						table_line.y1 = vlines_1d[i].y1
						grouped_vlines.append(table_line)
						table_line = TableLine(0,0,0,0)
						table_line.x0 = vlines_1d[i+1].x0
						table_line.x1 = vlines_1d[i+1].x1
						table_line.y0 = vlines_1d[i+1].y0
						table_line.y1 = vlines_1d[i+1].y1
				grouped_vlines.append(table_line)

			# # group the vlines with similar x0 and similar y0 & y1
			# for 
		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			logger.error("[ERROR] Fail to group the vlines with error message: [{}] in line [{}]".format(str(e), exc_tb.tb_lineno))
			raise e

		logger.debug("end - grouped_vlines length=[{}]".format(len(grouped_vlines)))
		return grouped_vlines

	def _group_hlines(hlines):
		logger.debug("start")
		hdict = dict()

		for hline in hlines:
			if hline.y1 in hdict:
				# if y1 existed in hdict
				if hline.x0 < hdict[hline.y1]["x0"]:
					hdict[hline.y1]["x0"] = hline.x0 # replace the min value of x0
				if hline.x1 > hdict[hline.y1]["x1"]:
					hdict[hline.y1]["x1"] = hline.x1 # replace the max value of x1
			else:
				hdict[hline.y1] = dict()
				hdict[hline.y1]["x0"] = hline.x0 # initialize the min value of x0
				hdict[hline.y1]["y0"] = hline.y0
				hdict[hline.y1]["x1"] = hline.x1 # initialize the max value of x1
				hdict[hline.y1]["y1"] = hline.y1

		grouped_hline = []

		for key in hdict:
			grouped_hline.append(TableLine(
					hdict[key]["x0"],
					hdict[key]["y0"],
					hdict[key]["x1"],
					hdict[key]["y1"]
				))
		grouped_hline = sorted(grouped_hline, key=operator.attrgetter('y1'), reverse=True)
		logger.debug("end - grouped_hlines length=[{}]".format(len(grouped_hline)))
		return grouped_hline

	def _parse_hlines(self):
		hlines = []
		for lt_obj in self.layout:
			if isinstance(lt_obj, LTCurve):
				h = int(lt_obj.height)
				w = int(lt_obj.width)
				if h == 0 and w != 0:
					hlines.append(lt_obj)
		return hlines

	def _parse_vlines(self):
		vlines = []
		for lt_obj in self.layout:
			if isinstance(lt_obj, LTCurve):
				h = int(lt_obj.height)
				w = int(lt_obj.width)
				if h != 0 and w == 0:
					vlines.append(lt_obj)
		return vlines










#!/usr/bin/python3
# -*- coding: utf-8 -*-
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from io import StringIO
from enum import Enum
import sys
import logging


logger = logging.getLogger("tableminer")

class ReportType(Enum):
	CUMMULATIVE = 'cummulative'
	QUARTER = 'quarter'

class ReportUtil:
	def _get_page_content(page):
		codec = 'utf-8'
		rsrcmgr = PDFResourceManager()
		sio = StringIO()

		laparams = LAParams()
		device = TextConverter(rsrcmgr, sio, codec=codec, laparams=laparams)
		interpreter = PDFPageInterpreter(rsrcmgr, device)
		interpreter.process_page(page)
		text = sio.getvalue()

		# Cleanup
		device.close()
		sio.close()

		return text

	# get the text content of the {page} in the {pdfname}
	def get_report_type(pdfname):
		logger.debug("start")
		try:
			fp = open(pdfname, 'rb')
			for page in PDFPage.get_pages(fp):
				# get the first page of the pdf
				page_content = ReportUtil._get_page_content(page)
				print ("front page_content=[{}]".format(page_content))
				if ('半年度报告' in page_content or '前三季度' in page_content or '年度报告' in page_content):
					report_type =  ReportType.CUMMULATIVE.value
				else:
					report_type = ReportType.QUARTER.value
				break # read the first page only

			logger.debug("end")
			return report_type
		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			logger.error("get type failed with error message: [{}] in line [{}]".format(str(e), exc_tb.tb_lineno))
			return None

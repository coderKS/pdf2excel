#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
import os
from datetime import datetime

class Logger:
	# debug: boolean
	# filepath: string
	#
	# if debug is true, will log to filepath and console
	# else log to console only
	def __init__(self, debug=False, filepath=None):
		self.logFormatter = logging.Formatter('%(asctime)s [%(levelname)-5.5s] [%(filename)s:%(lineno)s]  %(funcName)s# %(message)s')
		self.debug = debug
		self.filepath = filepath
		self.filename = None
		if self.debug:
			self.filename = "log_" + datetime.now().strftime("%Y%m%d-%H:%M:%S")
			self._make_directory()

	# make the filepath directory if not exists
	def _make_directory(self):
		if self.filepath == None:
			return
		if not os.path.exists(self.filepath):
			try:
				os.makedirs(self.filepath)
			except OSError as e:
				if e.errno != errno.EEXIST:
					raise

	# set the logging config
	# for details, refer to [https://stackoverflow.com/questions/13733552/logger-configuration-to-log-to-file-and-print-to-stdout]
	def setLogger(self):
		rootLogger = logging.getLogger("tableminer")
		rootLogger.setLevel(logging.DEBUG)

		if self.debug:
			# fileHandler: log to file 
			fileHandler = logging.FileHandler("{0}{1}.log".format(self.filepath, self.filename))
			fileHandler.setFormatter(self.logFormatter)
			fileHandler.setLevel(logging.DEBUG)
			rootLogger.addHandler(fileHandler)

		# consoleHandler: log to sysout
		consoleHandler = logging.StreamHandler()
		consoleHandler.setFormatter(self.logFormatter)
		consoleHandler.setLevel(logging.INFO)
		rootLogger.addHandler(consoleHandler)



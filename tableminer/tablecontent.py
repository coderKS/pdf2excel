import logging
import sys, os
import collections
from tableminer.tablelayout import TableLine
from tableminer.textbox import TextBox
from tableminer.constants import Constants
from tableminer.util import Util
logger = logging.getLogger("tableminer")
class Indentation:
	def __init__(self):
		self.x0_dict = dict()

	def _update_level(self):
		# sort the levels dict by the key, allocate the level value accordingly
		level_counter = 0
		od = collections.OrderedDict(sorted(self.x0_dict.items()))
		for k, v in od.items(): 
			self.x0_dict[k] = level_counter
			level_counter += 1
			logger.debug("k=[{}], v=[{}]".format(k,v))

	def _is_exist(self, x0):
		x0 = int(x0)
		return (x0 in self.x0_dict or
			x0 - Constants.CONSTANTS_INDENTATION_MARGIN in self.x0_dict or
			x0 + Constants.CONSTANTS_INDENTATION_MARGIN in self.x0_dict)

	def put(self, x0):
		x0 = int(x0)
		if self._is_exist(x0):
			return

		self.x0_dict[int(x0)] = -1
		self._update_level()

	def get_indentation(self, x0):
		x0 = int(x0)
		if not self._is_exist(x0):
			return 0

		if (x0 in self.x0_dict):
			return self.x0_dict[x0]
		elif (x0 - Constants.CONSTANTS_INDENTATION_MARGIN in self.x0_dict):
			return self.x0_dict[x0 - Constants.CONSTANTS_INDENTATION_MARGIN]
		else:
			return self.x0_dict[x0 + Constants.CONSTANTS_INDENTATION_MARGIN]

	def print_info(self):
		logger.debug("dict size=[{}]".format(len(self.x0_dict)))
		for key in self.x0_dict:
			logger.debug("dict[{}]={}".format(key, self.x0_dict[key]))

class TableCell:
	def __init__(self):
		self.textboxes = [] # list of textboxes
		self.row_index = -1 # int 
		self.col_index = -1	# int
		self.level = 0 # indentation level


class TableContent:
	def __init__(self):
		self.table_info = None
		self.table = [] # an 2D array of Table Cell
		self.start_page = None
		self.end_page = None
		# this is a ugly approach to store the set of used textbox
		# @TODO, check interaction between TableLine and TextBox
		self.used_texts = set()
		self.indentation = Indentation() 

	def set_start_page(self, start_page):
		self.start_page = start_page

	def set_end_page(self, end_page):
		self.end_page = end_page

	def set_table_info(self, info):
		self.table_info = info

	# set the level value (indentation value) for each table cell
	# by looking into their x0 value
	# before that, need to convert the space character into the x-coordinate width
	# now, 1 space = 4.5 unit width
	# this value could be adjusted in the constants file
	def set_indentation(self):
		logger.debug("start")
		# merge the textboxes
		try:
			for i in range(1, len(self.table)):
				# skip the first row
				row = self.table[i]
				

				first_cell = row[0]
				if (len(first_cell.textboxes) == 0):
					continue

				if Util.is_row_empty(row):
					continue
				# merge the first textboxes (i.e [" "," "," abcd","efgh"] -> ["abcd","efgh"])
				first_cell.textboxes = Util.merge_first_textboxes(first_cell.textboxes)
				# debug
				if (len(first_cell.textboxes)):
					self.indentation.put(first_cell.textboxes[0].x0)

			# debug the indentation
			self.indentation.print_info()

			# set the level to each first cell
			for i in range(1, len(self.table)):
				# skip the first row
				row = self.table[i]
				# get the first cell in the row
				first_cell = row[0]
				if len(first_cell.textboxes) == 0:
					continue
				# get the first textbox in the cell
				first_textbox = first_cell.textboxes[0]
				first_cell.level = self.indentation.get_indentation(first_textbox.x0)
		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			logger.error("set indentation fail with error message: [{}] in line [{}]".format(str(e), exc_tb.tb_lineno))
			raise e

	# sometimes the table has the page header hlines, this method try to remove those page header hlines
	# if there are no textboxes in between the first hlines and second hlines
	def _remove_header_hlines(self, hlines, vlines, textboxes):
		if len(hlines) > 1:
			for i in range(len(vlines) - 1):
				x0 = vlines[i].x0
				y0 = hlines[0].y0
				x1 = vlines[i+1].x1
				y1 = hlines[1].y1
				targetbox = TextBox(x0, y0, x1, y1)
				for textbox in textboxes:
					if textbox.get_key() in self.used_texts:
						continue

					if targetbox.is_interacts(textbox):
						return
				
				# end for each textboxes
			# end for vlines
			# remove the first hlines
			logger.debug("No textboxes interacted in between hlines[0] and hlines[1]. Remove first hline")
			hlines.pop(0)

	def add_content(self, hlines, vlines, textboxes):
		try:
			# remove header hlines
			self._remove_header_hlines(hlines, vlines, textboxes)
			
			# Test adding a left most line to see if left most line and vlines[0] contains text
			#      and a right most line to see if right most line and vlines[-1] contains text
			if (len(vlines) > 0 and len(hlines) > 1):
				x0 = Constants.CONSTANTS_PDF_PAGE_LEFT
				y0 = hlines[0].y0
				x1 = vlines[0].x1 - 2.5
				y1 = hlines[1].y1
				leftbox = TextBox(x0, y0, x1, y1)

				x0 = vlines[-1].x0 + 2.5
				x1 = Constants.CONSTANTS_PDF_PAGE_RIGHT
				rightbox = TextBox(x0, y0, x1, y1)

				for textbox in textboxes:

					if textbox.get_key() in self.used_texts or textbox.text.strip(" ") == "":
						continue

					if leftbox.is_interacts(textbox) and len(textbox.text.strip(" ")) > 1:
						# add additional checking. If the interacted text is '-' or length of 1, ignore it
						logger.debug("detected textbox [{}] interacted with leftbox [{}]".format(textbox, leftbox))
						leftline = TableLine(Constants.CONSTANTS_PDF_PAGE_LEFT, 
							vlines[0].y0, 
							Constants.CONSTANTS_PDF_PAGE_LEFT,
							vlines[0].y1)
						vlines.insert(0, leftline)

					if rightbox.is_interacts(textbox) and len(textbox.text.strip(" ")) > 1:
						logger.debug("detected textbox [{}] interacted with rightbox [{}]".format(textbox, rightbox))
						rightline = TableLine(Constants.CONSTANTS_PDF_PAGE_RIGHT,
							vlines[-1].y0,
							Constants.CONSTANTS_PDF_PAGE_RIGHT,
							vlines[-1].y1)
						vlines.append(rightline)

			row_index = len(self.table)
			for i in range(len(hlines)-1):
				row = []
				col_index = 0
				
				for j in range(len(vlines)-1):
					cell = TableCell()
					cell.col_index = col_index # set col index
					cell.row_index = row_index # set row index

					x0 = vlines[j].x0
					y0 = hlines[i].y0
					x1 = vlines[j+1].x1
					y1 = hlines[i+1].y1
					targetbox = TextBox(x0, y0, x1, y1)

					for textbox in textboxes:
						if textbox.get_key() in self.used_texts:
							continue

						if targetbox.is_interacts(textbox):
							cell.textboxes.append(textbox)
							self.used_texts.add(textbox.get_key())

					# end for each textbox
					row.append(cell)

					col_index += 1 # increment the col_index
				
				# end for each vlines
				if not Util.is_row_empty(row):
					# if the row is not empty, append the row in the table
					self.table.append(row)
					row_index += 1 # increment the row_index

		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			logger.error("add content fail with error message: [{}] in line [{}]".format(str(e), exc_tb.tb_lineno))
			raise e


	def print_table(self):
		logger.debug("###################################")

		for row in self.table:
			row_text = []
			for cell in row:
				cell_text = "".join(textbox.text for textbox in cell.textboxes)
				row_text.append(cell_text)
			logger.debug("# {0}".format(row_text))

		logger.debug("###################################")








#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging

logger = logging.getLogger("tableminer")

class TextBox:
	def __init__(self, x0, y0, x1, y1, text=None, page=None):
		self.x0 = x0
		self.y0 = y0
		self.x1 = x1
		self.y1 = y1
		self.text = text
		self.istopic = False
		self.page = page

		# if text contains space between words, then it is a topic line sentence
		if (text != None and len(text.strip(" ").split(" ")) > 1):
			self.istopic = True
		return
		
	def __repr__(self):
		return "textbox=[ x0=[{}], y0=[{}], x1=[{}], y1=[{}], text=[{}]]".format(
				self.x0, self.y0, self.x1, self.y1,	self.text)

	def print_box(self):
		print("textbox=[ x0=[{}], y0=[{}], x1=[{}], y1=[{}], text=[{}]]".format(
				self.x0, self.y0, self.x1, self.y1,	self.text
			))
	
	def get_key(self):
		return self.text + str(self.x0) + str(self.y0) + str(self.x1) + str(self.y1) + \
						str(self.page) if self.page else "" # hash the page if the page is not none 
		
	def is_interacts(self, other_box):
		# self:
		#
		# 0-----------
		# |           |
		# |           |
		#  -----------1 
		#

		# other_box
		# 0-----------
		# |           |
		# |           |
		#  -----------1 
		#
		# boxA = self.box
		# boxB = other_box.box
		_is_interacts =  not (
			self.y1 >= other_box.y0 or
			self.y0 <= other_box.y1 or 
			self.x0 >= other_box.x1 or
			self.x1 <= other_box.x0 
			# boxA[1]["y"] >= boxB[0]["y"] or
			# boxA[0]["y"] <= boxB[1]["y"] or
			# boxA[0]["x"] >= boxB[1]["x"] or
			# boxA[1]["x"] <= boxB[0]["x"]
		) 

		# the width of the textbox should not exceed the width of the layout box by 20
		# let other_box = textbox, self box = layout box
		if other_box.x1 - other_box.x0 > self.x1 - self.x0 + 20:
			_is_interacts = False


		if (_is_interacts):
			logger.debug("box[%6d, %6d, %6d, %6d] interacts with [%6d, %6d, %6d, %6d]: text[%s]" % 
					(
						self.x0, self.y0, self.x1, self.y1,
						other_box.x0, other_box.y0, other_box.x1, other_box.y1,
						other_box.text
					)
				)
		return _is_interacts

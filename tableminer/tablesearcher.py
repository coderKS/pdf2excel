#!/usr/bin/python
# -*- coding: utf-8 -*-
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from tableminer.constants import Constants
from io import StringIO
import logging

logger = logging.getLogger("tableminer")

class Index:
	def __init__(self, page_num, index):
		self.page_num = page_num
		self.index = index

class EndIndex(Index):
	def __init__(self, page_num, index):
		super(self.__class__, self).__init__(page_num, index)

class StartIndex(Index):
	def __init__(self, page_num, index):
		super(self.__class__, self).__init__(page_num, index)

class Table:
	def __init__(self, name, startIndexes, endIndexes, suffix):
		self.name = name 									# name of the table
		self.startIndexes = startIndexes  # list of start index
		self.endIndexes = endIndexes 			# list of end index
		self.suffix = suffix							# suffix
		logger.debug("name=[{0}], startIndexes=[{1}], endIndexes=[{2}], suffix=[{3}]".format(
			self.name, self.startIndexes, self.endIndexes, self.suffix
		))

class TableSearchResult:
	def __init__(self, table):
		self.table = table
		self.startIndexPages = []		# list of page containing start index
		self.endIndexPages = []			# list of page containing end index
		self.startPage = -1					# table start page
		self.endPage = -1						# table end page
		self.isFound = False				# table is found in the pdf of not
		self.indexes = []

	def filter_indexes(self):
		logger.debug("start")

		min_diff = 999999999 # max num
		table_start_page = 0
		table_end_page = 0
		# print the indexes
		
		for i in range(len(self.indexes)):
			# for each start index in indexes
			if not type(self.indexes[i]) is StartIndex:
				continue

			for j in range(i+1, len(self.indexes)):
				# for each end indexes in indexes behind the start index
				if not type(self.indexes[j]) is EndIndex:
					continue

				if self.indexes[j].page_num - self.indexes[i].page_num < min_diff:
					table_start_page = self.indexes[i].page_num
					table_end_page = self.indexes[j].page_num
					min_diff = self.indexes[j].page_num - self.indexes[i].page_num

		if table_start_page == 0 and table_end_page == 0 or table_end_page - Constants.CONSTANTS_TABLE_MAX_PAGES + 1 > table_start_page:
			self.isFound = False
		else:
			self.isFound = True
			self.startPage = table_start_page
			self.endPage   = table_end_page

		logger.debug("min_diff=[{0}], isFound=[{1}], startPage=[{2}], endPage=[{3}]".format(
			min_diff, self.isFound, self.startPage, self.endPage))
		logger.debug("end")

class TableSearcher:
	def __init__(self, pdfname):
		self.pdfname = pdfname
		self.fp = open(pdfname, 'rb')

	# get the text content of the {page} in the {pdfname}
	def _get_page_content(self, pdfname, page):
		codec = 'utf-8'
		rsrcmgr = PDFResourceManager()
		sio = StringIO()

		laparams = LAParams()
		device = TextConverter(rsrcmgr, sio, codec=codec, laparams=laparams)
		interpreter = PDFPageInterpreter(rsrcmgr, device)
		interpreter.process_page(page)
		text = sio.getvalue()

		# Cleanup
		device.close()
		sio.close()

		return text

	# parse the page content into an array of indexes
	def _parse_page_content_into_indexes(content, page_num, table):
		search_found = True
		indexes = []
		min_index_val = 999999999
		min_index = None
		next_begin = 0
		startIndexes = table.startIndexes
		endIndexes = table.endIndexes

		while(search_found):
			min_index_val = 999999999
			search_found = False

			# find the start index in the content
			for index in startIndexes:
				ret_index = content.find(index, next_begin)
				if ret_index == -1:
					# startIndex not found
					continue
				if ret_index < min_index_val:
					# startIndex is found and the index value is smaller than min_index
					logger.info("Table[{}]: start_index [{}] is found in page[{}]".format(table.name, index, page_num))
					min_index_val = ret_index
					min_index = StartIndex(page_num, index)
					search_found = True

			# find the end index in the content
			for index in endIndexes:
				ret_index = content.find(index, next_begin)
				if ret_index == -1:
					# endIndex not found
					continue
				if ret_index < min_index_val:
					# endIndex is found and the index value is smaller than min_index
					logger.info("Table[{}]: end_index [{}] is found in page[{}]".format(table.name, index, page_num))
					min_index_val = ret_index
					min_index = EndIndex(page_num, index)
					search_found = True

			if (search_found):
				# append the min_index into return indexes array
				indexes.append(min_index)
				# update the next_begin value
				next_begin = min_index_val + 1

		return indexes

	# tables: list of Table
	#	return list of TableSearchResult
	def search_tables(self, tables):
		current_page = 1
		searchResults = []
		for table in tables:
			searchResults.append(TableSearchResult(table))

		try:
			for page in PDFPage.get_pages(self.fp):
				logger.debug("Searching in page[%d]" % current_page)

				# get the content of the page
				page_content = self._get_page_content(self.pdfname, page)
				# logger.debug("Page content: {}".format(page_content))

				# for each table:
				for i in range(len(tables)):
					page_indexes = TableSearcher._parse_page_content_into_indexes(page_content, current_page,	tables[i])
					for index in page_indexes:
						searchResults[i].indexes.append(index)

				# increment currect_page
				current_page += 1
		except Exception as e:
			logger.error("[ERROR] Fail to read file: [%s] with error message: [%s]" % (self.pdfname, str(e)))
			return None

		for result in searchResults:
			result.filter_indexes()

		# filter search result and return
		logger.debug("end")
		return searchResults
#!/usr/bin/python3
import logging
import csv
import re
from tableminer.tablewriter import ExcelWriter

logger = logging.getLogger("tableminer")

class Translation:
	def __init__(self):
		self.dictionary = dict()
		self.row_items = []

	def set_dictionary(self, file_paths):
		for fpath in file_paths:
			with open(fpath, 'r', encoding='utf-8') as csvfile:
				reader = csv.reader(csvfile, delimiter=',')
				next(reader, None)
				for row in reader:
					chinese = row[3]
					english = row[4]
					if chinese in self.dictionary:
						continue
					else:
						self.dictionary[chinese] = english

	def add_row_items(self, tablecontent):
		for row in tablecontent.table:
			if len(row) == 0:
				logger.warn("length of row is zero")
				continue
			row_cell = row[0]
			if (len(row_cell.textboxes) == 0):
				logger.warn("length of the row_cell is zero")
				continue
			cell_text = "".join([textbox.text for textbox in row_cell.textboxes])
			cell_text = self._normalize(cell_text)
			logger.debug("cell_text: {}".format(cell_text))
			self.row_items.append(cell_text)

	def _get_result(self):
		result = []

		for item in self.row_items:
			row = []
			row.append(item)
			if item in self.dictionary:
				row.append(self.dictionary[item])
			else:
				row.append("")
			result.append(row)
		return result

	def _normalize(self, text):
		# remove the number chinese character
		chars_to_remove = ['一','二','三','四','五','六','七','八','九','十',
												'1','2','3','4','5','6','7','8','9']
		sc = set(chars_to_remove)
		text = ''.join([c for c in text if c not in sc])

		# remove the special character and space
		text = ''.join(e for e in text if e.isalnum())
		return text

	def export_result(self, output_file_path):
		result = self._get_result()
		writer = ExcelWriter(output_file_path)
		writer.writerow(["Chinese","Translation in English"])
		for row in result:
			writer.writerow(row)



#!/usr/bin/env python
# -*- coding: utf-8 -*-
# converts a pdf into a csv file
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from tableminer.tablelayout import TableLayout, TableLine
from tableminer.tablecontent import TableContent
from tableminer.textbox import TextBox
from tableminer.constants import Constants
import sys, os
import logging

logger = logging.getLogger("tableminer")

class TableParser:
	def get_textbox_by_index(textboxes, indexes):
		for index in indexes:
			for textbox in textboxes:
				if index in textbox.text:
					logger.debug("Index: {} is found in textbox[text=[{}], x0=[{}], y0=[{}], x1=[{}], y1=[{}]]"
						.format(index, textbox.text, textbox.x0, textbox.y0, textbox.x1, textbox.y1))
					return textbox

		logger.warn("Indexes: {} not found in the textboxes".format(indexes))
		return None

	def parse_region(textboxes, x0, y0, x1, y1):
		logger.debug("start")
		region_textboxes = []

		targetbox = TextBox(x0, y0, x1, y1)
		for textbox in textboxes:
			if targetbox.is_interacts(textbox):
				region_textboxes.append(textbox)

		logger.debug("end")
		return region_textboxes

	def filter_lines_less_than(lines, x0=None, y0=None, x1=None, y1=None):
		if x0 == None and y0 == None and x1 == None and y1 == None:
			return lines
		if x0 != None:
			lines = list(filter(lambda line : line.x0 >= x0, lines))
		if x1 != None:
			lines = list(filter(lambda line : line.x1 >= x1, lines))
		if y0 != None:
			lines = list(filter(lambda line : line.y0 >= y0, lines))
		if y1 != None:
			lines = list(filter(lambda line : line.y1 >= y1, lines))
		return lines

	def filter_lines_larger_than(lines, x0=None, y0=None, x1=None, y1=None):

		if x0 == None and y0 == None and x1 == None and y1 == None:
			return lines
		if x0 != None:
			lines = list(filter(lambda line : line.x0 <= x0, lines))
		if x1 != None:
			lines = list(filter(lambda line : line.x1 <= x1, lines))
		if y0 != None:
			lines = list(filter(lambda line : line.y0 <= y0, lines))
		if y1 != None:
			lines = list(filter(lambda line : line.y1 <= y1, lines))
		return lines

	# return a tablecontent
	def parse_table_content(filepath, search_result):
		logger.debug("start")
		table = search_result.table
		startIndexes = table.startIndexes
		endIndexes = table.endIndexes
		pages = range(search_result.startPage, search_result.endPage+1)
		fp = open(filepath, 'rb')
		parser = PDFParser(fp)
		doc = PDFDocument(parser)
		table_content = TableContent() # object to be returned
		table_content.set_start_page(search_result.startPage)
		table_content.set_end_page(search_result.endPage)
		try:
			for (pageno, page) in enumerate(PDFPage.create_pages(doc)):
				if(not (pageno + 1) in pages):
					continue
				logger.debug("\tpageno=[%d]" % (pageno+1))
				layout    = TableLayout(page, pageno+1)
				hlines 		= layout.hlines # Table horizontal lines
				vlines 		= layout.vlines # Table vertical lines
				textboxes = layout.texts  # Text Box in the page

				# find the title textbox in the start page
				if pageno + 1 == search_result.startPage:
					logger.debug("Finding the title textbox in the start page")
					# get the text above the table
					title_textbox = TableParser.get_textbox_by_index(textboxes, startIndexes)
					if not title_textbox:
						# if title cannot be found, return None
						logger.warn("Title [{}] text box not found in the start page [{}]".format(startIndexes, pageno + 1))
						return None

					# if title textbox is found, extract the info above the title textbox

					# find the first horizontal line below the title textbox
					# @TODO: error handling
					filtered_hlines = TableParser.filter_lines_larger_than(hlines, y0=title_textbox.y0)
					if (len(filtered_hlines) == 0):
						logger.debug("No hlines below title_textbox. Skip this page")
						continue

					first_hline = TableParser.filter_lines_larger_than(hlines, y0=title_textbox.y0)[0]

					logger.debug("First Line: [x0=[{}],y0=[{}],x1=[{}],y1=[{}]".format(first_hline.x0,
												first_hline.y0,
												first_hline.x1,
												first_hline.y1))
					logger.debug("Title [{}] text box is found in the start page".format(startIndexes))
					table_info_textboxes = TableParser.parse_region(textboxes,  
																				Constants.CONSTANTS_PDF_PAGE_LEFT, # x0
																				title_textbox.y0 + Constants.CONSTANTS_TABLE_INFO_PADDING, # y0,
																				Constants.CONSTANTS_PDF_PAGE_RIGHT, # x1
																				first_hline.y0 # y1
																			)
					# set table info
					table_info_text = ",".join(textbox.text for textbox in table_info_textboxes)
					table_content.table_info = table_info_text
					logger.debug("Set table info: {}".format(table_info_text))

					# filter out the vertical lines
					logger.debug("before filtering out all the vlines")
					for vline in vlines:
						logger.debug("\t\tx0=[{0}], y0=[{1}], x1=[{2}], y1=[{3}]".format(vline.x0, vline.y0, vline.x1, vline.y1))
					logger.debug("before filtering out all the hlines")
					for hline in hlines:
						logger.debug("\t\tx0=[{0}], y0=[{1}], x1=[{2}], y1=[{3}]".format(hline.x0, hline.y0, hline.x1, hline.y1))
					
					vlines = TableParser.filter_lines_larger_than(vlines, y0=title_textbox.y1+1) # y1 is the start y-coord
					hlines = TableParser.filter_lines_larger_than(hlines, y0=title_textbox.y1+1) # y0 is the end   y-coord
					
					logger.debug("after filtering out all the vlines")
					for vline in vlines:
						logger.debug("\t\tx0=[{0}], y0=[{1}], x1=[{2}], y1=[{3}]".format(vline.x0, vline.y0, vline.x1, vline.y1))
					logger.debug("after filtering out all the hlines")
					for hline in hlines:
						logger.debug("\t\tx0=[{0}], y0=[{1}], x1=[{2}], y1=[{3}]".format(hline.x0, hline.y0, hline.x1, hline.y1))
				
				# locate the end of table in the end page
				if pageno + 1 == search_result.endPage:
					logger.debug("Finding the table end in the end page")
					# find the textboxes if end indexes
					end_textbox = TableParser.get_textbox_by_index(textboxes, endIndexes)
					if not end_textbox:
						logger.warn("End [{}] text box not found in the end page [{}]".format(endIndexes, pageno + 1))
						return None


					logger.debug("End Text box [{}]is found in the end page".format(endIndexes))

					# find the first vertical lines that y1 of the line > y1 of the end text
					# get the y0 of the first vertical lines
					# ...
					# (y1)
					# |         |          |          | 
					# |         |          |          |
					# | (y1)    |          |          |
					# | end text| 100,000  |          |
					# |         |          |          |
					# |         |          |          |
					#  ------------------------------- (x1, y0)
					first_vertical_line = None
					for vline in vlines:
						if vline.y1 > end_textbox.y1:
							first_vertical_line = vline

					if not first_vertical_line:
						logger.warn("First Vertical Line not found")
						continue

					logger.debug("first veritical line = [x0=[{}, y0=[{}], x1=[{}], y1=[{}]]".format(
											first_vertical_line.x0,
											first_vertical_line.y0,
											first_vertical_line.x1,
											first_vertical_line.y1
									))
					logger.debug("before filtering out all the vlines")
					for vline in vlines:
						logger.debug("\t\tx0=[{0}], y0=[{1}], x1=[{2}], y1=[{3}]".format(vline.x0, vline.y0, vline.x1, vline.y1))
					logger.debug("before filtering out all the hlines")
					for hline in hlines:
						logger.debug("\t\tx0=[{0}], y0=[{1}], x1=[{2}], y1=[{3}]".format(hline.x0, hline.y0, hline.x1, hline.y1))
					vlines = TableParser.filter_lines_less_than(vlines, y1=first_vertical_line.y0-1)
					hlines = TableParser.filter_lines_less_than(hlines, y1=first_vertical_line.y0-1)
					logger.debug("after filtering out all the vlines")
					for vline in vlines:
						logger.debug("\t\tx0=[{0}], y0=[{1}], x1=[{2}], y1=[{3}]".format(vline.x0, vline.y0, vline.x1, vline.y1))
					logger.debug("after filtering out all the hlines")
					for hline in hlines:
						logger.debug("\t\tx0=[{0}], y0=[{1}], x1=[{2}], y1=[{3}]".format(hline.x0, hline.y0, hline.x1, hline.y1))
				# add table content
				table_content.add_content(hlines, vlines, textboxes)

		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			logger.error("[ERROR] Fail to parse csv file: [{}] with error message: [{}] in line: [{}]".format(filepath, str(e), exc_tb.tb_lineno))
		
		# after adding the content, set the indentation of the whole table
		table_content.set_indentation()
		return table_content


	# filepath: string
	# search_result: TableSearchResult
	# NOT USED ANYMORE
	def parseTable(self, filepath, search_result):
		logger.debug("start")
		startIndexes = search_result.table.startIndexes
		endIndexes = search_result.table.endIndexes
		pages = range(result.startPage, result.endPage+1)
		fp = open(filepath, 'rb')
		parser = PDFParser(fp)
		doc = PDFDocument(parser)
		canStartWrite = False
		canStopWrite = False
		parsedResult = ParsedResult(search_result.table)

		try:
			for (pageno, page) in enumerate(PDFPage.create_pages(doc)):
				logger.debug("\tpageno=[%d]" % (pageno+1))
				if(not (pageno + 1) in pages):
					continue
				layout = TableLayout(page)
				hlines, vlines = layout.get_coordinates()
				i=0
				while(i<len(vlines)-1):
					j=0
					row=[]
					row_content = layout.get_row(hlines[0], hlines[-1], vlines[i], vlines[i+1])
					logger.debug("row_content=[%s]" % row_content)
					# continue if the trimmed row_content is empty
					if len(row_content.strip(" ")) == 0:
						i += 1
						continue
					# check whether the endIndex in the content
					if canStartWrite and endIndex in row_content:
						logger.debug("Can Stop Write now...")
						canStopWrite = True
						fp.close()
						return
					# check whether the startIndex in the content
					elif not canStartWrite and startIndex in row_content:
						logger.debug("Can Start Write now...")
						canStartWrite = True
					# continue start index not found
					elif not canStartWrite:
						i += 1
						continue

					while(j<len(hlines)-1):
						if not hlines[j+1]-hlines[j]>10:
							j=j+1
							continue
						record_content = ' '.join(layout.get_region(hlines[j]+1, vlines[i], hlines[j+1]-1, vlines[i+1]))
						logger.debug("record_content=["+record_content+"]")
						row.append(record_content)
						j=j+1
					parsedResult.add_row(row)
					i=i+1
			fp.close()
		except Exception as e:
			logger.error("[ERROR] Fail to parse csv file: [%s] with error message: [%s]" % (pdf, str(e)))
		logger.debug("end")





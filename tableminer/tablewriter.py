#!/usr/bin/python
# -*- coding: utf-8 -*-
from io import StringIO
import sys, csv, codecs
import xlsxwriter
import logging

logger = logging.getLogger("tableminer")

class CsvWriter:
	def __init__(self, filename, dialect=csv.excel, encoding="utf-8", **kwds):
		self.queue = StringIO()
		self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
		self.stream = open(filename, "w+")
		self.encoder = codecs.getincrementalencoder(encoding)()

	def writerow(self, row):
		logger.debug("start")
		logger.debug("row=[{0}]".format(row))

		self.writer.writerow(row)
		# Fetch UTF-8 output from the queue ...
		data = self.queue.getvalue()
		# write to the target stream
		self.stream.write(data)
		# empty queue
		self.queue.truncate(0)
		logger.debug("end")

	# list of string
	def writerows(self, rows):
		for row in rows:
			self.writerow(row)

	def close(self):
		self.writer.close()

class ExcelWriter:
	def __init__(self, filename):
		self.workbook = xlsxwriter.Workbook(filename)
		self.worksheet = self.workbook.add_worksheet()
		self.currentRow = 0

	# row: list of string
	def writerow(self, row):
		logger.debug("start")
		logger.debug("row=[{0}]".format(row))
		
		self.worksheet.write_row(self.currentRow,0, row)
		self.currentRow += 1
		logger.debug("end")

	def writerows(self, rows):
		for row in rows:
			self.writerow(row)
	
	def close(self):
			self.workbook.close()

class TableWriter:

	def __init__(self, output_file_path, tablecontent, output_format="csv"):
		self.tablecontent = tablecontent

		if not tablecontent:
			logger.error("TableContent not found")
			return

		if output_format == "csv":
			self.writer = CsvWriter(output_file_path)

		elif output_format == "xlsx":
			self.writer = ExcelWriter(output_file_path)

		else:
			logger.error("Invalid output format: {0}, Only csv or xlsx is supported".format(output_format))

	def _get_cell_text(cell):
		# calculate the indenetation
		cell_text = ""
		cell_text += "    " * cell.level
		cell_text += "".join([textbox.text for textbox in cell.textboxes])

		return cell_text

	def export(self):
		logger.debug("start")
		if not self.tablecontent:
			logger.error("TableContent is empty")
			return

		if not self.writer:
			logger.error("Writer not found")
			return

		# write the table header
		self.writer.writerow(['Table Header',self.tablecontent.table_info])

		# write table content
		for row in self.tablecontent.table:
			row_content = []
			for cell in row:
				cell_text = TableWriter._get_cell_text(cell)
				logger.debug("cell_text: {} is in {} level".format(cell_text, cell.level))
				row_content.append(cell_text)
			self.writer.writerow(row_content)

		# write start page end page
		self.writer.writerow(['Start Page', self.tablecontent.start_page])
		self.writer.writerow(['End Page', self.tablecontent.end_page])
		self.writer.close()
		logger.debug("end")




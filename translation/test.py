#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Imports the Google Cloud client library
from google.cloud import translate
from oauth2client.client import GoogleCredentials
credentials = GoogleCredentials.get_application_default()

# Instantiates a client
translate_client = translate.Client()

# The text to translate
text = u'存货增加'
# The target language
target = 'en'

# Translates some text into Russian
translation = translate_client.translate(
  text,
	target_language=target
)

print(u'Text: {}'.format(text))
print(u'Translation: {}'.format(translation['translatedText']))


# from google.cloud import storage

# # If you don't specify credentials when constructing the client, the
# # client library will look for credentials in the environment.
# storage_client = storage.Client()

# # Make an authenticated API request
# buckets = list(storage_client.list_buckets())
# print(buckets)


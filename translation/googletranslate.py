#!/usr/bin/python
# -*- coding: utf-8 -*-
from googletrans import Translator
translator = Translator()
print (translator.translate('You may wonder why this library works properly, whereas other approaches such like goslate won’t work since Google has updated its translation service recently with a ticket mechanism to prevent a lot of crawler programs.', dest='zh-TW').text)

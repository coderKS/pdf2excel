#!/usr/bin/python3
# -*- coding: utf-8 -*-
import pymysql
import configparser
import csv
from google.cloud import translate
from oauth2client.client import GoogleCredentials

def isValid(d):
	result = 'ticker' in d and d['ticker'].isdigit() and 'exchange' in d and d['exchange'] != '-'
	return result

def upload_to_mysql(data, source='csv'):
	for d in data:
		sql = """INSERT INTO TRANSLATION (ticker, text, translated_text, from_lang, to_lang, exchange, source) VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}')""".format(d['ticker'], d['text'], d['translated_text'], d['from_lang'], d['to_lang'], d['exchange'], source)
		print ("sql = %s" % (sql))
		try:
			cursor.execute(sql)
			db.commit()	
		except Exception as e:
			print (e)
			print ("Error: unable to add data")

def google_translation(data):
	for d in data:
		translation = translate_client.translate(
		  d['text'],
			target_language='en'
		)
		d['translated_text'] = translation['translatedText']

	return data

# google translate
credentials = GoogleCredentials.get_application_default()
translate_client = translate.Client()


#db
config = configparser.ConfigParser()
config.sections()
config.read('../db/db.ini')
db_address = config['db']['address']
db_port = config['db']['port']
db_username = config['db']['username']
db_password = config['db']['password']
db_name = config['db']['dbname']
db = pymysql.connect(db_address, db_username, db_password, db_name, charset='utf8')
cursor = db.cursor()


data = []
sql = """SELECT DISTINCT item FROM ITEM_VALUE"""
print ("sql = %s" % (sql))

cursor.execute(sql)
results = cursor.fetchall()
for row in results:
	d = dict()
	item = row[0]
	d['from_lang'] = 'zh-cn'
	d['to_lang'] = 'en'
	d['exchange'] = 'all'
	d['ticker'] = 'all'
	d['text'] = item
	d['translated_text'] = None
	data.append(d)
		
data = google_translation(data)
print (data)
upload_to_mysql(data, source='google-translate')

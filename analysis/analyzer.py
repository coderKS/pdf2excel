#!/usr/bin/python
# -*- coding: utf-8 -*-
from query import Query
from io import StringIO
import sys, csv, codecs
import xlsxwriter
import logging
import json
import numpy as np

logger = logging.getLogger("analysis")

class Analyzer:
	def __init__(self, query):
		self.query = query

	def get_table_rows(self, company_name, table_name, report_type):
		if table_name == '现金流量表补充资料':
			data = self.query.report_supp_query(company_name, table_name)
		elif report_type == 'annual':
			data = self.query.report_annual_query(company_name, table_name)
		elif report_type == 'quarter':
			data = self.query.report_query(company_name, table_name)

		all_cols_items = []
		for period in data:
			for col_item in data[period]['col_items']:
				all_cols_items.append((period, col_item))

		rows = []
		header = ['']
		for period, item in all_cols_items:
			header.append(str(period) + " " + item)
		rows.append(header)
		logger.debug("header={}\n".format(header))

		# find the latest period
		max_period, max_period_item = all_cols_items[-1]


		"""
		# now assume row_item name won't be duplicated
		"""

		# get the row_items of the latest period

		latest_row_items = data[max_period]['row_items']

		# get the row_items that are not in the latest period but exist in ealier period
		other_row_items = []
		for period in data:
			if period == max_period:
				continue # skip the latest period
			for item, row_index, level in data[period]['row_items']:
				if not Analyzer.exist_in_row_items(item, latest_row_items) and not Analyzer.exist_in_row_items(item, other_row_items):
					other_row_items.append((item, row_index, level))

		logger.debug("latest_row_items={}".format(latest_row_items))
		logger.debug("other_row_items={}".format(other_row_items))

		# all_row_items = latest_row_items + other_row_items
		for row_item, row_index, level in latest_row_items:
			row = []
			row.append(Analyzer.get_text(row_item, level))
			for period, col_item in all_cols_items:
				row.append(Analyzer.get_value(row_item, data[period][col_item], data[period]['row_items']))
			rows.append(row)

		other_rows = []
		for row_item, row_index, level in other_row_items:
			row = []
			row.append(Analyzer.get_text(row_item, level))
			for period, col_item in all_cols_items:
				row.append(Analyzer.get_value(row_item, data[period][col_item], data[period]['row_items']))
			other_rows.append(row)

		return (rows, other_rows)

	def get_text(row_name, level):
		return "    "*level + row_name

	def get_value(target_item, col_item, row_items):
		index = None
		for row_item, row_index, level in row_items:
			if (target_item == row_item):
				index = row_index
		if (index == None):
			return "N/A"
		else:
			return col_item[index]

	def exist_in_row_items(target_item, row_items):
		for row_item, row_index, level in row_items:
			if (target_item == row_item):
				return True
		return False




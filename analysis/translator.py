import logging
from util import Util

logger = logging.getLogger("analysis")

class Translator:
	def __init__(self, connectionManager):
		self.connectionManager = connectionManager

	def translate(self, word, from_lang='zh-cn', to_lang='en'):
		db = self.connectionManager.get_connection()
		word = word.strip(" ")
		cursor = db.cursor()
		query = """SELECT translated_text FROM pdfminer.TRANSLATION 
		where text = '{}' and from_lang='{}' and to_lang='{}';""".format(word, from_lang, to_lang)
		logger.debug("sql = %s" % (query))
		try:
			cursor.execute(query)
			results = cursor.fetchall()
			for row in results:
				translated_text = row[0]
				return translated_text
		except Exception as e:
			print (e)
			print ("Error: unable to fetch data")
		logger.debug("word not found")
		return None
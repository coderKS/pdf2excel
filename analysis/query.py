#!/usr/bin/python3
import pymysql
import re
import logging

logger = logging.getLogger("analysis")


# get the query object
class QueryFactory:
	def __init__(self, connection_manager):
		self.connection_manager = connection_manager

	def get_query(self, type):
		if (type == 'yearly'):
			return AnnualQuery(self.connection_manager)
		elif (type == 'supp'):
			return SuppQuery(self.connection_manager)
		elif (type == 'quarter'):
			return QuarterQuery(self.connection_manager)
		else:
			return None

class Query:
	def __init__(self, connection_manager):
		self.connection_manager = connection_manager
	# return the data 
	def make_query(self, company_name, table_name):
		raise NotImplementedError

class QuarterQuery(Query):
	def make_query(self, company_name, table_name):
		db = self.connection_manager.get_connection()
		cursor = db.cursor()
		query = """SELECT iv.item, iv.value_content, iv.column_item, iv.sequence, ap.year, ap.start_period FROM pdfminer.ITEM_VALUE iv, pdfminer.REPORT_TABLE rt, pdfminer.ANNUAL_REPORT ap, pdfminer.Company c 
WHERE iv.table_id = rt.table_id AND rt.report_id = ap.report_id AND ap.company_id = c.company_id
AND c.company_name = '{}' AND rt.table_name = '{}' 
ORDER BY ap.file_name, iv.sequence;""".format(company_name, table_name)
		print ("sql = %s" % (query))
		data = []
		try:
			cursor.execute(query)
			results = cursor.fetchall()
			for row in results:
				row_data = dict()
				row_data['item'] = row[0]
				row_data['value'] = row[1]
				row_data['column'] = row[2]
				row_data['sequence'] = row[3]
				row_data['year'] = row[4]
				row_data['quarter'] = row[5]
				data.append(row_data)
			return data

		except Exception as e:
			print (e)
			print ("Error: unable to fetch data")

		return None

class SuppQuery(Query):
	def make_query(self, company_name, table_name):
		db = self.connection_manager.get_connection()
		cursor = db.cursor()
		query = """SELECT iv.item, iv.value_content, iv.column_item, iv.sequence, ap.year, ap.start_period FROM pdfminer.ITEM_VALUE iv, pdfminer.REPORT_TABLE rt, pdfminer.ANNUAL_REPORT ap, pdfminer.Company c 
WHERE iv.table_id = rt.table_id AND rt.report_id = ap.report_id AND ap.company_id = c.company_id
AND c.company_name = '{}' AND rt.table_name = '{}' AND ( ap.start_period = '4Q' or ap.start_period = '2Q' )
ORDER BY ap.file_name, iv.sequence;""".format(company_name, table_name)
		print ("sql = %s" % (query))
		data = []
		try:
			cursor.execute(query)
			results = cursor.fetchall()
			for row in results:
				row_data = dict()
				row_data['item'] = row[0]
				row_data['value'] = row[1]
				row_data['column'] = row[2]
				row_data['sequence'] = row[3]
				row_data['year'] = row[4]
				row_data['quarter'] = row[5]
				data.append(row_data)
			return data

		except Exception as e:
			print (e)
			print ("Error: unable to fetch data")

		return None

class AnnualQuery(Query):
	def make_query(self, company_name, table_name):
		db = self.connection_manager.get_connection()
		cursor = db.cursor()
		query = """SELECT iv.item, iv.value_content, iv.column_item, iv.sequence, ap.year, ap.start_period FROM ITEM_VALUE iv, REPORT_TABLE rt, ANNUAL_REPORT ap, Company c 
WHERE iv.table_id = rt.table_id AND rt.report_id = ap.report_id AND ap.company_id = c.company_id
AND c.company_name = '{}' AND rt.table_name = '{}' AND ap.start_period = '4Q'
ORDER BY ap.file_name, iv.sequence;""".format(company_name, table_name)
		data = []
		try:
			cursor.execute(query)
			results = cursor.fetchall()
			for row in results:
				row_data = dict()
				row_data['item'] = row[0]
				row_data['value'] = row[1]
				row_data['column'] = row[2]
				row_data['sequence'] = row[3]
				row_data['year'] = row[4]
				row_data['quarter'] = row[5]
				data.append(row_data)
			return data

		except Exception as e:
			print (e)
			print ("Error: unable to fetch data")

		return None

# class ColumnFactory:
# 	def create_column_by_sequence(col_name, sequence):
# 		last_5_digit = sequence[-5:]
# 		return Column(col_name, int(last_5_digit), sequence)

# class RowItem:
# 	def __init__(col_name, level, sequence):
# 		self.col_name = col_name
# 		self.level = level
# 		self.sequence = sequence

# class RowItemFactory:
# 	def create_item(col_name, sequence):
# 		last_5_digit = sequence[-5:]
# 		return RowItem(col_name, int(last_5_digit), sequence)

# class Query:
# 	def __init__(self, connection_manager):
# 		self.connection_manager = connection_manager

# 	# def _normalize(self, text):
# 	# 	# remove the number chinese character
# 	# 	chars_to_remove = ['一','二','三','四','五','六','七','八','九','十',
# 	# 											'1','2','3','4','5','6','7','8','9']
# 	# 	sc = set(chars_to_remove)
# 	# 	text = ''.join([c for c in text if c not in sc])

# 	# 	# remove the special character and space
# 	# 	text = ''.join(e for e in text if e.isalnum())
# 	# 	return text

# 	def report_annual_query(self, company_name, table_name):
# 		db = self.connection_manager.get_connection()
# 		cursor = db.cursor()
# 		query = """SELECT iv.item, iv.value_content, iv.column_item, iv.sequence, ap.year FROM ITEM_VALUE iv, REPORT_TABLE rt, ANNUAL_REPORT ap, Company c 
# WHERE iv.table_id = rt.table_id AND rt.report_id = ap.report_id AND ap.company_id = c.company_id
# AND c.company_name = '{}' AND rt.table_name = '{}' AND ap.start_period = '4Q'
# ORDER BY ap.file_name, iv.sequence;""".format(company_name, table_name)
# 		print ("sql = %s" % (query))
# 		data = {}
# 		# col_data = dict()
# 		# row_data = dict()
# 		# col_arr = []
# 		try:
# 			cursor.execute(query)
# 			results = cursor.fetchall()
# 			report_data = {}
# 			for row in results:
# 				item = row[0]
# 				value = row[1]
# 				column_item = row[2]
# 				sequence = row[3]
# 				year = int(row[4])
# 				level = int(sequence[-5:])
# 				row_index = int(sequence[5:-5])
# 				# print ("item={}, value={}, column_item={}, sequence={}, year={}".format(item, value, column_item, sequence, year))
# 				if not year in data:
# 					data[year] = {
# 						'row_items': [],
# 						'col_items': []
# 					}

# 				if not column_item in data[year]['col_items']:
# 					data[year]['col_items'].append(column_item)
# 					data[year][column_item] = {}

# 				row_set = (item, row_index, level)
# 				if not row_set in data[year]['row_items']:
# 					data[year]['row_items'].append(row_set)

# 				if not row_index in data[year][column_item]:
# 					data[year][column_item][row_index] = value

# 			return data

# 		except Exception as e:
# 			print (e)
# 			print ("Error: unable to fetch data")

# 		return None

# 	def report_query(self, company_name, table_name):
# 		db = self.connection_manager.get_connection()
# 		cursor = db.cursor()
# 		query = """SELECT iv.item, iv.value_content, iv.column_item, iv.sequence, ap.year, ap.start_period FROM pdfminer.ITEM_VALUE iv, pdfminer.REPORT_TABLE rt, pdfminer.ANNUAL_REPORT ap, pdfminer.Company c 
# WHERE iv.table_id = rt.table_id AND rt.report_id = ap.report_id AND ap.company_id = c.company_id
# AND c.company_name = '{}' AND rt.table_name = '{}' 
# ORDER BY ap.file_name, iv.sequence;""".format(company_name, table_name)
# 		print ("sql = %s" % (query))
# 		data = {}
# 		# col_data = dict()
# 		# row_data = dict()
# 		# col_arr = []
# 		try:
# 			cursor.execute(query)
# 			results = cursor.fetchall()
# 			report_data = {}
# 			for row in results:
# 				item = row[0]
# 				value = row[1]
# 				column_item = row[2]
# 				sequence = row[3]
# 				year = int(row[4])
# 				quarter = row[5]

# 				level = int(sequence[-5:])
# 				row_index = int(sequence[5:-5])
# 				period = str(year)+'-'+quarter
# 				if not period in data:
# 					data[period] = {
# 						'row_items': [],
# 						'col_items': []
# 					}

# 				if not column_item in data[period]['col_items']:
# 					data[period]['col_items'].append(column_item)
# 					data[period][column_item] = {}

# 				row_set = (item, row_index, level)
# 				if not row_set in data[period]['row_items']:
# 					data[period]['row_items'].append(row_set)

# 				if not row_index in data[period][column_item]:
# 					data[period][column_item][row_index] = value

# 			return data

# 		except Exception as e:
# 			print (e)
# 			print ("Error: unable to fetch data")

# 		return None

# 	# query for 现金流量表补充资料
# 	def report_supp_query(self, company_name, table_name):
# 		db = self.connection_manager.get_connection()
# 		cursor = db.cursor()
# 		query = """SELECT iv.item, iv.value_content, iv.column_item, iv.sequence, ap.year, ap.start_period FROM pdfminer.ITEM_VALUE iv, pdfminer.REPORT_TABLE rt, pdfminer.ANNUAL_REPORT ap, pdfminer.Company c 
# WHERE iv.table_id = rt.table_id AND rt.report_id = ap.report_id AND ap.company_id = c.company_id
# AND c.company_name = '{}' AND rt.table_name = '{}' AND ( ap.start_period = '4Q' or ap.start_period = '2Q' )
# ORDER BY ap.file_name, iv.sequence;""".format(company_name, table_name)
# 		print ("sql = %s" % (query))
# 		data = {}
# 		# col_data = dict()
# 		# row_data = dict()
# 		# col_arr = []
# 		try:
# 			cursor.execute(query)
# 			results = cursor.fetchall()
# 			report_data = {}
# 			for row in results:
# 				item = row[0]
# 				value = row[1]
# 				column_item = row[2]
# 				sequence = row[3]
# 				year = int(row[4])
# 				quarter = row[5]

# 				level = int(sequence[-5:])
# 				row_index = int(sequence[5:-5])
# 				period = str(year)+'-'+quarter
# 				if not period in data:
# 					data[period] = {
# 						'row_items': [],
# 						'col_items': []
# 					}

# 				if not column_item in data[period]['col_items']:
# 					data[period]['col_items'].append(column_item)
# 					data[period][column_item] = {}

# 				row_set = (item, row_index, level)
# 				if not row_set in data[period]['row_items']:
# 					data[period]['row_items'].append(row_set)

# 				if not row_index in data[period][column_item]:
# 					data[period][column_item][row_index] = value

# 			return data

# 		except Exception as e:
# 			print (e)
# 			print ("Error: unable to fetch data")

# 		return None



import numpy as np
import logging
from constants import Constants
logger = logging.getLogger("analysis")

class Report:
	def __init__(self):
		self.headers = []
		self.row_items = []
		self.main_contents = []

class ReportBuilder:
	def __init__(self):
		self.report = Report()

	def get_period(self):
		return self.report.headers[1]

	def get_end_dates(self):
		return self.report.headers[2]

	def make_text(self, text, level):
		return "    "*level + text

	def build(self, data):
		if (len(data) == 0):
			return None
		self.build_header(data)
		self.build_row_items(data)
		self.build_main_content(data)
		return self.report

	def make_header(self, year, quarter):
		raise NotImplementedError

	def can_convert_number(self, val):
		# logger.info('val={}, type={}'.format(val, type(val)))
		if (val == None):
			return False
		if (isinstance(val, float)):
			return True
		try:
			float(val.replace(',', ''))
			return True
		except ValueError:
			return False
		

	def convert_number(self, val):
		if (isinstance(val, float)):
			return val
		return float(val.replace(',',''))

	def adjust_by_column(self, column, year, quarter):
		if (column == None):
			logger.warn("column is None")
			return
		else:
			column = column.strip(" ")

		for word in Constants.CONSTANTS_IGNORE:
			if word in column:
				logger.warn("Ignore" + column)
				return
				
		for word in Constants.CONSTANTS_PREVIOUS_QUARTER_WORDS:
			if word in column:
				return year-1, quarter

		for word in Constants.CONSTANTS_PREVIOUS_ANNUAL_WORDS:
			if word in column:
				return year-1, '4Q'  

		for word in Constants.CONSTANTS_CURRENT_QUARTER_WORDS:
			if word in column:
				return year, quarter

		logger.warn("Can't insert the value, column not found: " + column)
		return year, quarter

	def build_header(self, data):
		end_dates_dict = {
			'1Q': 'Mar 31, ',
			'2Q': 'June 30, ',
			'3Q': 'Sep 31, ',
			'4Q': 'Dec 31, '
		}
		end_dates = []

		period = []
		for row in data:
			quarter = row['quarter']
			year = row['year']
			header = self.make_header(year, quarter)
			if header == None:
				continue
			if not header in period:
				period.append(header)
				end_dates.append(end_dates_dict.get(quarter) + year)

		self.report.headers.append([])
		self.report.headers.append(period)
		self.report.headers.append(end_dates)
		self.report.headers.append([])
		logger.debug(self.report.headers)

	def build_row_items(self, data):
		# parse row
		raise NotImplementedError

	def build_main_content(self, data):
		# parse the main content
		raise NotImplementedError

	def append_formula_row(self, data):
		raise NotImplementedError

class CumulativeReportBuilder(ReportBuilder):

	def make_header(self, year, quarter):
		d = {
			'1Q': '3M',
			'2Q': '6M',
			'3Q': '9M',
			'4Q': '12M'
		}
		if d.get(quarter) == None:
			return None
		return 'First ' + d.get(quarter) + ' ' + year

	def get_year_quarter(self, header):
		d = {
			'3M': '1Q',
			'6M': '2Q',
			'9M': '3Q',
			'12M': '4Q'
		}
		year = header.split(' ')[-1]
		quarter = d[header.split(' ')[1]]
		return (year, quarter)

	def build_row_items(self, data):
		# use the row items in the latest year
		latest_quarter = self.get_period()[-1]
		year, quarter = self.get_year_quarter(latest_quarter)
		mem = set()
		for row in data:
			if (row['year'] == year and row['quarter'] == quarter and 
				not row['item'] in mem):
					level = int(row['sequence'][10:])
					self.report.row_items.append(self.make_text(row['item'], level))
					mem.add(row['item'])

		self.report.row_items.append(Constants.CONSTANTS_SEPARATOR)
		for row in data:
			if (not row['item'] in mem):
				level = int(row['sequence'][10:])
				self.report.row_items.append(self.make_text(row['item'], level))
				mem.add(row['item'])

		logger.debug(self.report.row_items)

	def insert_value(self, data):
		year = int(data['year'])
		quarter = data['quarter']
		column = data['column']
		item = data['item']
		value = data['value']
		
		header = self.make_header(str(year), quarter)
		if header == None:
			return
		year, quarter = self.adjust_by_column(column, year, quarter)
		
		logger.debug("column=["+column+"], header=["+header+"], item=["+item+"], value=["+str(value)+"]")
		col_index = None
		row_index = None
		# TODO: create a map for  searching the col_index
		for i in range(len(self.get_period())):
			if (self.get_period()[i].strip(" ") == header.strip(" ")):
				col_index = i
				break
		# TODO: create a map for searching the row_index
		for j in range(len(self.report.row_items)):
			if (self.report.row_items[j].strip(" ") == item.strip(" ")):
				row_index = j
				break
		if (col_index == None or row_index == None):
			return

		if (quarter == '3Q'):
			second_quarter_value = self.report.main_contents[row_index][col_index-1]
			if (self.can_convert_number(second_quarter_value) and self.can_convert_number(value)):
				second_quarter_number = self.convert_number(second_quarter_value)
				third_quarter_number = self.convert_number(value)

				self.report.main_contents[row_index][col_index] = second_quarter_number + third_quarter_number
				return

		self.report.main_contents[row_index][col_index] = value

	def build_main_content(self, data):
		# create  the 2d array
		self.report.main_contents = [['' for i in range(len(self.get_period()))] for j in range(len(self.report.row_items))]
		for d in data:
			self.insert_value(d)

		logger.debug(self.report.main_contents)

	def append_formula_row(self, data):
		return

class YearlyReportBuilder(ReportBuilder):
	def make_header(self, year, quarter):
		return year+'A'

	def build_row_items(self, data):
		# use the row items in the latest year
		latest_year = self.get_period()[-1]
		year = latest_year[:-1]
		mem = set()
		for row in data:
			if (row['year'] == year and 
				not row['item'] in mem):
					level = int(row['sequence'][10:])
					self.report.row_items.append(self.make_text(row['item'], level))
					mem.add(row['item'])

		self.report.row_items.append(Constants.CONSTANTS_SEPARATOR)
		for row in data:
			if (not row['item'] in mem):
				level = int(row['sequence'][10:])
				self.report.row_items.append(self.make_text(row['item'], level))
				mem.add(row['item'])

		logger.debug(self.report.row_items)

	def insert_value(self, data):
		year = int(data['year'])
		quarter = data['quarter']
		column = data['column']
		item = data['item']
		value = data['value']
		
		year, quarter = self.adjust_by_column(column, year, quarter)

		header = self.make_header(str(year), quarter)
		logger.debug("column=["+column+"], header=["+header+"], item=["+item+"], value=["+str(value)+"]")
		col_index = None
		row_index = None
		# TODO: create a map for  searching the col_index
		for i in range(len(self.get_period())):
			if (self.get_period()[i].strip(" ") == header.strip(" ")):
				col_index = i
				break
		# TODO: create a map for searching the row_index
		for j in range(len(self.report.row_items)):
			if (self.report.row_items[j].strip(" ") == item.strip(" ")):
				row_index = j
				break
		if (col_index == None or row_index == None):
			return

		self.report.main_contents[row_index][col_index] = value

	def build_main_content(self, data):
		# create  the 2d array
		self.report.main_contents = [['' for i in range(len(self.get_period()))] for j in range(len(self.report.row_items))]
		for d in data:
			self.insert_value(d)

		logger.debug(self.report.main_contents)

	def append_formula_row(self, data):
		return

class QuarterReportBuilder(ReportBuilder):
	def make_header(self, year, quarter):
		return quarter + ' '  + year

	def build_row_items(self, data):
		# use the row items in the latest year
		latest_quarter = self.get_period()[-1]
		quarter, year  = latest_quarter.split(' ')
		mem = set()
		for row in data:
			if (row['year'] == year and row['quarter'] == quarter and 
				not row['item'] in mem):
					level = int(row['sequence'][10:])
					self.report.row_items.append(self.make_text(row['item'], level))
					mem.add(row['item'])

		self.report.row_items.append(Constants.CONSTANTS_SEPARATOR)
		for row in data:
			if (not row['item'] in mem):
				level = int(row['sequence'][10:])
				self.report.row_items.append(self.make_text(row['item'], level))
				mem.add(row['item'])

		logger.debug(self.report.row_items)

	def insert_value(self, data):
		year = int(data['year'])
		quarter = data['quarter']
		column = data['column']
		item = data['item']
		value = data['value']
		
		header = self.make_header(str(year), quarter)
		year, quarter = self.adjust_by_column(column, year, quarter)

		logger.debug("column=["+column+"], header=["+header+"], item=["+item+"], value=["+str(value)+"]")
		col_index = None
		row_index = None
		# TODO: create a map for  searching the col_index
		for i in range(len(self.get_period())):
			if (self.get_period()[i].strip(" ") == header.strip(" ")):
				col_index = i
				break
		# TODO: create a map for searching the row_index
		for j in range(len(self.report.row_items)):
			if (self.report.row_items[j].strip(" ") == item.strip(" ")):
				row_index = j
				break
		if (col_index == None or row_index == None):
			return

		if (quarter == '2Q'):
			first_quarter_value = self.report.main_contents[row_index][col_index-1]
			if (self.can_convert_number(first_quarter_value) and self.can_convert_number(value)):
				first_quarter_number = self.convert_number(first_quarter_value)
				second_quarter_number = self.convert_number(value)
				self.report.main_contents[row_index][col_index] = second_quarter_number - first_quarter_number
				return

		elif (quarter == '4Q'):
			first_quarter_value = self.report.main_contents[row_index][col_index-3]
			second_quarter_value = self.report.main_contents[row_index][col_index-2]
			third_quarter_value = self.report.main_contents[row_index][col_index-1]
			if (self.can_convert_number(first_quarter_value) and self.can_convert_number(second_quarter_value) and self.can_convert_number(third_quarter_value) and self.can_convert_number(value)):
				first_quarter_number = self.convert_number(first_quarter_value)
				second_quarter_number = self.convert_number(second_quarter_value)
				third_quarter_number = self.convert_number(third_quarter_value)
				forth_quarter_number = self.convert_number(value)
				self.report.main_contents[row_index][col_index] = forth_quarter_number - third_quarter_number - second_quarter_number - first_quarter_number
				return

		self.report.main_contents[row_index][col_index] = value

	def build_main_content(self, data):
		# create  the 2d array
		self.report.main_contents = [['' for i in range(len(self.get_period()))] for j in range(len(self.report.row_items))]
		for d in data:
			self.insert_value(d)

		logger.debug(self.report.main_contents)

	def append_formula_row(self, data):
		return


class ReportConvertor:
	# parsing the report object into a 2d array
	# return the 2d array 
	# return None if parsing fails	
	def parse_array(report):
		if (report == None):
			return None
		arr = []
		for header in report.headers:
			arr.append([''])
			arr[-1].extend(header)
		for i in range(len(report.row_items)):
			arr.append([report.row_items[i]])
			arr[-1].extend(report.main_contents[i])
		return arr
import logging
import pymysql

logger = logging.getLogger("analysis")

class ConnectionManager:
	def __init__(self, address, port, username, password, dbname):
		self.address 	= address
		self.port	 		= port
		self.username = username
		self.password = password
		self.dbname   = dbname
		self.connection = self._create_connection()
	
	def _create_connection(self):
		db = pymysql.connect(self.address, self.username, self.password, self.dbname, charset='utf8')
		return db

	def get_connection(self):
		if (self.connection):
			return self.connection
		self.connection = self._create_connection();
		return self.connection

	def close_connection(self):
		if (self.connection):
			self.connection.close()
		self.connection = None
		



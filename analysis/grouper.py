#!/usr/bin/python3
import logging
import re
from util import Util
logger = logging.getLogger("analysis")
class RowGrouper:
	def __init__(self, splitter):
		self.splitter = splitter

	def group(self, report):
		raise NotImplementedError

class SimilarRowGrouper(RowGrouper):

	def is_similar(self, text1, text2):
		return text1 != '' and text2 != '' and Util.normalize(text1) == Util.normalize(text2)

	def copy_row(self, row, other_row):
		for i in range(1, len(row)):
			if (row[i] == 'N/A'):
				row[i] = other_row[i]

	def group(self, report):
		rows, other_rows = splitter.split(report)
		grouped_rows = []
		grouped_other_rows = []
		for row in rows:
			for other_row in other_rows:
				if (self.is_similar(row[0], other_row[0])):
					grouped_other_rows.append(other_row)
					self.copy_row(row, other_row)
					# end for len(row)
			# end for other_row
			grouped_rows.append(row)
		# end for row
		grouped_other_rows = [r for r in other_rows if r not in grouped_other_rows]
		report = splitter.merge(grouped_rows, grouped_other_rows)
		return report

class SameMeaningRowGrouper(RowGrouper):

	def find_in_items(self, text1, text2, items):
		return Util.normalize(text1) in items and Util.normalize(text2) in items

	def copy_row(self, row, other_row):
		for i in range(1, len(row)):
			if (row[i] == 'N/A'):
				row[i] = other_row[i]

	def group(self, report):
		rows, other_rows = splitter.split(report)
		# set similar_item
		similar_item = 
		grouped_rows = []
		grouped_other_rows = []

		for row in rows:
			for other_row in other_rows:
				for items in similar_items:
					if (self.find_in_items(row[0], other_row[0], similar_items)):
						grouped_other_rows.append(other_row)
						self.copy_row(row, other_row)
			grouped_rows.append(row)
		# end for row
		grouped_other_rows = [r for r in other_rows if r not in grouped_other_rows]
		report = splitter.merge(grouped_rows, grouped_other_rows)
		return report


class Splitter:
	def split(self, report):
		rows = []
		other_rows = []
		arr = [['']]
		arr[0].extend(report.headers)
		for i in range(len(report.row_items)):
			arr.append([report.row_items[i]])
			arr[-1].extend(report.main_contents[i])
		
		after_separator = False
		for row in arr:
			if (not after_separator and row[0] == QuarterReportBuilder.separator):
				after_separator = True
				continue
			if not after_separator:
				rows.append(row)
			else:
				other_rows.append(row)
			
		
		return rows, other_rows


	
	def merge(self, rows, other_rows):


		
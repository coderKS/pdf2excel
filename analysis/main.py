import logging
import configparser
from analyzer import Analyzer
from tablewriter import TableWriter
from connectionManager import ConnectionManager
from logger import Logger
from query import QueryFactory, QuarterQuery, SuppQuery, AnnualQuery
from report import Report, QuarterReportBuilder, YearlyReportBuilder, CumulativeReportBuilder, ReportConvertor
from validator import Validator
import json
import codecs
from util import Util
from xlsxwriter.utility import xl_rowcol_to_cell
from translator import Translator
import sys, os
_logger = Logger(debug=True, filepath='../log/')
_logger.setLogger()
logger = logging.getLogger("analysis")

def merge_report_arr(report_arr_1, report_arr_2):
	report_arr = []
	report_arr_1_row_num = len(report_arr_1)
	report_arr_2_row_num = len(report_arr_2)
	for i in range(report_arr_1_row_num):
		row = []
		row.extend(report_arr_1[i])
		row.append('')
		if (i < report_arr_2_row_num):
			row.extend(report_arr_2[i])
		report_arr.append(row)
	
	report_arr_1_col_num = len(report_arr_1[0])
	for i in range(report_arr_2_row_num - report_arr_1_row_num):
		index = report_arr_1_row_num + i
		row = []
		for j in range(report_arr_1_col_num):
			row.append('')
		row.append('')
		row.extend(report_arr_2[index])
		report_arr.append(row)	

	return report_arr

def get_left_side_bar(translator, table_name, row_items):
	d = {
		'合并资产负债表': 'Balance Sheet',
		'合并利润表': 'Income Statement',
		'合并现金流量表': 'Cash Flow Statement',
	}
	side_bar = []
	side_bar.append(['DCF value'])
	side_bar.append(['RMB'])
	side_bar.append(['Fiscal Period Ended Date'])
	side_bar.append([d.get(table_name)])
	for item in row_items:
		translated_text = translator.translate(item)
		side_bar.append([translated_text])

	logger.debug("side_bar=[{}]".format(side_bar))
	return side_bar

# get the setting
config = configparser.ConfigParser()
config.sections()
config.read('../db/db.ini')
db_address = config['db']['address']
db_port = config['db']['port']
db_username = config['db']['username']
db_password = config['db']['password']
db_name = config['db']['dbname']
db_conn = ConnectionManager(db_address, db_address, db_username, db_password, db_name)

# get the query object using factory
factory = QueryFactory(db_conn)

# get translator
translator = Translator(db_conn)


def generate_report(company_name, table_name):
	# try:
		file_type = '年度' if table_name == '合并现金流量表' else '季度'
		if (table_name == '合并现金流量表'):
			# get quarter report
			query = factory.get_query('supp')
			# execute the query
			result = query.make_query(company_name, table_name)
			# build the quarterly report 
			report_builder = YearlyReportBuilder()
			yearly_report = report_builder.build(result)
			side_bar = get_left_side_bar(translator, table_name, yearly_report.row_items)
			yearly_report_arr = ReportConvertor.parse_array(yearly_report)

			# merge report
			report_arr = merge_report_arr(side_bar, yearly_report_arr)

			# write the table
			writer = TableWriter(company_name, table_name, file_type)
			writer.export_rows(report_arr)
			 
		else:
			# get quarter report
			query = factory.get_query('quarter')

			# execute the query
			result = query.make_query(company_name, table_name)

			# build the quarterly report 
			report_builder = QuarterReportBuilder()
			quarter_report = report_builder.build(result)
			quarter_report_arr = ReportConvertor.parse_array(quarter_report)

			# build the side bar
			side_bar = get_left_side_bar(translator, table_name, quarter_report.row_items)

			# build the cumulative report
			report_builder = CumulativeReportBuilder()
			cumulative_report = report_builder.build(result)
			cumulative_report_arr = ReportConvertor.parse_array(cumulative_report)

			query = factory.get_query('yearly')
			result = query.make_query(company_name, table_name)

			# build the yearly report
			report_builder = YearlyReportBuilder()
			yearly_report = report_builder.build(result)
			yearly_report_arr = ReportConvertor.parse_array(yearly_report)
			
			# Validate
			validation_result = Validator.validate(yearly_report, quarter_report)
			
			# merge report
			report_arr = merge_report_arr(side_bar, yearly_report_arr)
			report_arr = merge_report_arr(report_arr, quarter_report_arr)
			report_arr = merge_report_arr(report_arr, cumulative_report_arr)

			Util.array_number_correction(report_arr, 1000000, ['基本每股收益', '稀释每股收益', '综合收益总额'])
			
			# write the table
			writer = TableWriter(company_name, table_name, file_type)
			writer.export_rows(report_arr)
			writer.export_rows(validation_result)
			writer.close()
	# except Exception as e:
	# 	exc_type, exc_obj, exc_tb = sys.exc_info()
	# 	filename = company_name+file_type+table_name
	# 	logger.error("[ERROR] Fail to generate report: [{}] with error message: [{}] in line: [{}]".format(filename, str(e), exc_tb.tb_lineno))
	# 	logger.error("trace=[{}]".format(e))

'''
#######################
#	Program starts here #
#######################
'''

# company_names = ['洋河股份', '歌尔声学', '片仔癀', '大族激光', '恒生电子', '科大讯飞', '科大讯飞']
company_names = ['洋河股份']
table_names = ['合并资产负债表', '合并利润表', '合并现金流量表']

for company in company_names:
	for table in table_names:
		generate_report(company, table)



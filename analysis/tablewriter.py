import logging
import xlsxwriter
logger = logging.getLogger("analysis")

class TableWriter:
	output_path = '../pdf/report_result/'
	def __init__(self, company_name, table_name, file_type):
		self.company_name = company_name
		self.table_name = table_name
		self.file_type = file_type
		self.filename = self.output_path+self.company_name+self.file_type+self.table_name+".xlsx"
		self.workbook = xlsxwriter.Workbook(self.filename)

	def export_rows(self, rows):
		worksheet = self.workbook.add_worksheet()
		currentRow = 0
		for row in rows:
			worksheet.write_row(currentRow, 0, row)
			currentRow += 1

	def close(self):
		self.workbook.close()



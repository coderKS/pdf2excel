import logging
from util import Util

logger = logging.getLogger("analysis")
class Validator:

	def validate(annual_report, quarter_report):
		annual_data = dict()
		for i in range(len(annual_report.row_items)):
			for j in range(len(annual_report.headers[1])):
				row = annual_report.row_items[i]
				col = annual_report.headers[1][j]
				year = col[0:4]
				val = annual_report.main_contents[i][j]
				index = row + year
				if index not in annual_data:
					annual_data[index] = 0
				if (Util.can_convert_number(val)):
					annual_data[index] += Util.convert_number(val)

		quarter_data = dict()
		for i in range(len(quarter_report.row_items)):
			for j in range(len(quarter_report.headers[1])):
				row = quarter_report.row_items[i]
				col = quarter_report.headers[1][j]
				year = col[-4:]
				val = quarter_report.main_contents[i][j]
				index = row + year
				if index not in quarter_data:
					quarter_data[index] = 0
				if (Util.can_convert_number(val)):
					quarter_data[index] += Util.convert_number(val)

		result = []
		result.append(['Item', 'Year', 'Annual Sum', 'Quarter Sum'])
		for key in annual_data:
			if not key in quarter_data:
				continue
			if (not quarter_data[key] == annual_data[key]):
				# logger.warn("Not Match: item=[{}], annual_value=[{}], sum of quarter value=[{}]".format(key, annual_data[key], quarter_data[key]))
				year = key[-4:]
				item = key[:-4]
				result.append([item, year, annual_data[key], quarter_data[key]])

		return result

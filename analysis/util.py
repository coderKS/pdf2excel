#!/usr/bin/python3
import logging
logger = logging.getLogger("analysis")
class Util:
	
	def get_config(file):
		return json.load(codecs.open(file, 'r', 'utf-8-sig'))

	def normalize(text):
		# print ("normalize# start")
		# remove the number chinese character
		text = "".join(text.split())
		chars_to_remove = ['一','二','三','四','五','六','七','八','九','十',
												'1','2','3','4','5','6','7','8','9']
		sc = set(chars_to_remove)
		text = ''.join([c for c in text if c not in sc])

		# remove the special character and space
		text = ''.join(e for e in text if e.isalnum())
		return text

	# AB123 => AB
	def remove_digit(s):
		return "".join([_s for _s in s if not _s.isdigit()]) 

	'''
	lookups:
	['row1']

	merged_rows:
	[
		['', 		'col1', 'col2'],
		['row1','100',  '200'],
		['row2','200',  '300']
	]

	return the row number if any lookup found in merged rows # 1
	return -1 otherwise
	'''
	def find_row_index(lookups, rows):
		i = 1
		for row in rows:
			for lookup in lookups:
				if (lookup in row[0]): # lookup found in row[0]
					return i
			i += 1
		return -1

	def can_convert_number(val):
		if (val == None):
			return False
		# logger.info('val={}, type={}'.format(val, type(val)))
		if (isinstance(val, float)):
			return True
		try:
			float(val.replace(',', ''))
			return True
		except ValueError:
			return False

	def convert_number(val):
		# logger.info('val={}, type={}'.format(val, type(val)))
		if (isinstance(val, float)):
			return val
		return float(val.replace(',', ''))

	def array_number_correction(arr, correction, targets):
		for i in range(len(arr)):
			for j in range(len(arr[i])):
				for target in targets:
					if (Util.can_convert_number(arr[i][j]) and target in str(arr[i][j])):
						corrected_number = Util.convert_number(arr[i][j]) / correction
						# logger.info('corrected_number: {}'.format(corrected_number))
						arr[i][j] = corrected_number


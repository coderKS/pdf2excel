#!/usr/bin/python3
import logging

class FormulaCompilationError(Exception):
	pass
class Formula:
	def __init__(self, formula):
		self.formula = formula

	'''
	text: "={a} + {b} - {c}"
	return replace text # "a"
	return None if '{' or '}' not found
	'''
	def _search_bracket_content(text):
		open_pos = text.find('{')
		if (open_pos == -1):
			return None
		close_pos = text.find('}')
		if (close_pos == -1):
			return None
		return text[open_pos+1 : close_pos]

	'''
	formula: "={operating_revenue}"
	map: 
	{
		"operating_revenue": 1
	}
	col: 'C'

	return the compiled formula # "=C1"

	'''
	def compile(self, items_map, col_index):
		formula = self.formula # copy the formula
		replace_text = Formula._search_bracket_content(formula)
		while(replace_text != None):
			if replace_text in items_map:
				row_index = items_map[replace_text] # 1
				target_text = '{'+replace_text+'}' 
				new_text = col_index + str(row_index) #C1
				formula = formula.replace(target_text, new_text) 
				replace_text = Formula._search_bracket_content(formula)
			else:
				raise FormulaCompilationError
		return formula

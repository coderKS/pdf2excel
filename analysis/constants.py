#!/usr/bin/python
# -*- coding: utf-8 -*-
class Constants:
	CONSTANTS_CURRENT_QUARTER_WORDS=['本期金额', '本期发生额', '期末余额']
	CONSTANTS_PREVIOUS_QUARTER_WORDS=['上期金额', '上期发生额', '上期金额发生额']
	CONSTANTS_PREVIOUS_ANNUAL_WORDS=['期初余额', '年初余额']
	CONSTANTS_SEPARATOR = "-----"
	CONSTANTS_IGNORE = ['年初至报告期期末金额', '上年年初至报告期期末金额']

	# 期末 for balance sheet cumulative
	# 期初岩

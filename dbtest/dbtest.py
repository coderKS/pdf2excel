from db.dao import CompanyDao
from db.connectionManager import ConnectionManager
import configparser
import unittest

class DBTest(unittest.TestCase):
	def when_read_config_should_get_value_of_db_props(self):
		print("when_read_config_should_get_value_of_db_props# start")
		config = configparser.ConfigParser()
		config.sections()
		config.read('db/db.ini')

		self.assertTrue('db' in config)
		self.assertTrue('address' in config["db"])
		self.assertTrue('port' in config["db"])
		self.assertTrue('username' in config["db"])
		self.assertTrue('password' in config["db"])
		print(config["db"]["address"])

if __name__ == '__main__':
	unittest.main()